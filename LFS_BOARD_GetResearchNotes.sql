USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER FUNCTION [dbo].[LFS_BOARD_GetResearchNotes](@cust_notes_no INT)
RETURNS VARCHAR(MAX) AS

-- Used to concatenate an ordered list of notes(VARCHAR(255)) into one notes field (VARCHAR(MAX))

BEGIN
	DECLARE @Notes VARCHAR(MAX)
	
	SELECT @Notes = COALESCE(@Notes + '', '') + notes
	FROM [TX_CUST_NOTES_EXT] 
	WHERE cust_notes_no = @cust_notes_no
	ORDER BY serial_order

	RETURN @Notes
END

GO
GRANT EXECUTE ON [dbo].[LFS_BOARD_GetResearchNotes] TO ImpUsers
GO