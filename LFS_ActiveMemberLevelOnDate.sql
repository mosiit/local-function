USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

--IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LFS_ActiveMemberLevelOnDate]') AND type IN (N'FN', N'IF', N'TF', N'FS', N'FT'))
--    DROP FUNCTION [dbo].[LFS_ActiveMemberLevelOnDate]
--GO

/*  LFS_ActiveMemberLevelOnDate
    Returns the membership level that was active for a particular customer on a particular date
 
        NOTE: If first date of the renewed membership, it uses the new membership number
              If membership bought later in day, it gives active member number for entire day
                    (no start of day option as with LFS_ActiveMemberOnDate function)
              If more than one membership is found, it returns the HIGHEST number
 
    There are three values passed to the function
       @customer_no = the customer to check
          @memb_org = a specific membership organization If 0, check all membership organizations
          @check_dt = Were they or will they be a valid member on this date?
      
    NOTE: Only memberships with a status of 1 (inactive), 2 (active) or 3 (pending) are looked at.
          Memberships with any other status like suspended, deactivated or merged will be ignored.
          
    Example:  SELECT [dbo].[LFS_ActiveMemberLevelOnDate] (67226, 4, '7-1-2018')  */

ALTER FUNCTION [dbo].[LFS_ActiveMemberLevelOnDate] (@customer_no INT, @memb_org INT, @check_dt DATETIME) RETURNS VARCHAR(50)
AS BEGIN 

    /*  Function Variables  */

        DECLARE @active_member_no INT = 0, @active_member_level VARCHAR(50) = NULL

        DECLARE @memb_table TABLE ([cust_memb_no] INT, [customer_no] INT, [memb_org_no] INT, [org_sort_no] INT,
                                  [memb_level] VARCHAR(10), [memb_level_name] VARCHAR(30), [start_amt] DECIMAL(18,2),
                                  [current_status] INT, [init_dt] DATETIME, [expr_dt] DATETIME)

        DECLARE @memb_orgs TABLE ([memb_org_no] INT, [org_sort_no] INT)

    /*  Check Parameters  */

        --Set @check_dt to today if null and set time to 00:00:00
        SELECT @check_dt = ISNULL(@check_dt,GETDATE())

        SELECT @check_dt = CONVERT(DATE,@check_dt)

        --Set Default Values
        SELECT @memb_org = ISNULL(@memb_org,0)

    /*  If Custome No = 0, Jump to the End  */

        IF @customer_no = 0 GOTO FINISHED

    /*  Set the Membership Organization Hierarchy  */
    
        INSERT INTO @memb_orgs ([memb_org_no],[org_sort_no])
        VALUES (24,10), (5,20), (26, 30), (4, 40), (10, 50), (13, 60), (27, 70), (7, 80), (8, 90), (9, 100)
        
    /*  Get all membership records for this customer - limit to specific organization if one was passed in
        Ignore records that aren't pending, active, or inactive - Remove times from all date/time values  */

        INSERT INTO @memb_table ([cust_memb_no],[customer_no],[memb_org_no],[org_sort_no],[memb_level],[memb_level_name],
                                [start_amt],[current_status],[init_dt],[expr_dt])
        SELECT mem.[cust_memb_no],
               mem.[customer_no], 
               mem.[memb_org_no], 
               ISNULL(org.[org_sort_no], 200),
               mem.[memb_level], 
               lev.[description],
               lev.[start_amt],
               mem.[current_status], 
               CAST(mem.[init_dt] AS DATE),
               CAST(mem.[expr_dt] AS DATE)
        FROM [dbo].[TX_CUST_MEMBERSHIP] AS mem
             INNER JOIN [dbo].[T_MEMB_LEVEL] AS lev ON lev.[memb_level] = mem.[memb_level]
             LEFT OUTER JOIN @memb_orgs AS org ON org.[memb_org_no] = mem.[memb_org_no]
        WHERE mem.[customer_no] = @customer_no
          AND (@memb_org = 0 OR mem.memb_org_no = @memb_org)
         AND mem.[current_status] IN (1, 2, 3, 9)   --1=Inactive/2=Active/3=Pending/9=Merged
     
    /*  If no memberships found for this customer, jump to the end  */

        IF NOT EXISTS (SELECT * FROM @memb_table) GOTO FINISHED

    /*  If looked specifically at household memberships, change staff membership to same organization as household memberships
        For the purposes of this function, the staff membership and household Membership organizations are the same  */

        IF @memb_org = 4 UPDATE @memb_table
                         SET [memb_org_no] = 4, [org_sort_no] = 70
                         WHERE [memb_org_no] = 27
 
    /* To determine if member that day, date passed must be between init date and expire date on an inactive, active, or pending membership  */

        SELECT @active_member_no = (SELECT TOP (1) [cust_memb_no]
                                    FROM @memb_table 
                                    WHERE @check_dt BETWEEN [init_dt] AND [expr_dt]
                                    ORDER BY [customer_no], [start_amt] DESC, [org_sort_no])

    /*  Deal with a null value - Default to 0 */

        SELECT @active_member_no = ISNULL(@active_member_no, 0)

    /*  */

        SELECT @active_member_level = ISNULL([memb_level_name], '(none)')
                                      FROM @memb_table
                                      WHERE [cust_memb_no] = @active_member_no

    
    FINISHED:

        /*  Return final value  */

            RETURN ISNULL(@active_member_level, '(none)')
            --SELECT @active_member_no, @active_member_level


END
GO

GRANT EXECUTE ON [dbo].[LFS_ActiveMemberLevelOnDate] TO ImpUsers, tessitura_app
GO

SELECT [dbo].[LFS_ActiveMemberOnDate]  (11044, 0, GETDATE(), 'N'), [dbo].[LFS_ActiveMemberLevelOnDate] (11044, 0, GETDATE())







