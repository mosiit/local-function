USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[LFS_BOARD_GetSpouse](@customer_no INT)
RETURNS VARCHAR(100) AS

BEGIN 

	DECLARE @spouse VARCHAR(200) = ''

	SELECT @spouse = ISNULL(c.fname + ' ' + c.lname, '')
	FROM dbo.V_CUSTOMER_WITH_HOUSEHOLD v
		INNER JOIN t_customer c
		ON v.expanded_customer_no = c.customer_no
	WHERE v.customer_no = @customer_no
	AND v.name_ind = -2

	RETURN @spouse

END 

GO