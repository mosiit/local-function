USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LFS_RUN_TIME_FROM_SECONDS]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
    DROP FUNCTION [dbo].[LFS_RUN_TIME_FROM_SECONDS]
GO

/*  pass it a number of seconds and it will return a displayable run time in the HH:MM:SS format  */

CREATE FUNCTION [dbo].[LFS_RUN_TIME_FROM_SECONDS] (@seconds DECIMAL(18,2)) Returns VARCHAR(25)
AS BEGIN

    DECLARE @minutes DECIMAL(18,2) = 0.0, @hours DECIMAL(18,2) = 0.0
    DECLARE @run_time VARCHAR(25) = ''

    IF @seconds < 0 SELECT @seconds = 0

    IF @seconds > 0 BEGIN

        SELECT @minutes = CAST((@seconds / 60) AS INT)

        SELECT @seconds = (@seconds - (@minutes * 60.0))

        SELECT @hours = CAST((@minutes / 60) AS INT)

        SELECT @minutes = (@minutes - (@hours * 60.0))
    
        SELECT @run_time = CONVERT(VARCHAR(15),CAST(@hours AS int)) + ':'
                         + CASE WHEN CAST(@minutes AS INT) < 10 THEN '0' ELSE '' END
                         + CONVERT(VARCHAR(2),CAST(@minutes AS INT)) + ':'
                         + CASE WHEN CAST(@seconds AS INT) < 10 THEN '0' ELSE '' END
                         + CONVERT(VARCHAR(2),CAST(@seconds AS INT))

    END

    FINISHED:

        RETURN IsNull(@run_time, '')

END
GO

GRANT EXECUTE ON [dbo].[LFS_RUN_TIME_FROM_SECONDS] TO [ImpUsers] AS [dbo]
GO
