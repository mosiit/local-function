USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LFS_STRING_CAP_CHECK]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
    DROP FUNCTION [dbo].[LFS_STRING_CAP_CHECK]
GO

    /*  Takes in a string and checks its capitalization, returning a code to indicate what it found
        
        The return code is a twelve character code (example (MXCSSCLC0128) and breaks down into four sections of 
        four characters, two characters, two charcters, and four characters.  The breakdown is as follows...

        MXCSHNSCLC0128 = MXCS SC LC 0128

        Characters 01 - 04:   UPCS = All Uppercase
                              LWCS = All Lowercase
                              MXCS = Mixed case (upper and lower)

        Characters 05 & 06:   SC = String Capitalization (first character in the string is capitalized)
                              WC = Words Capitalization (Each word in string (including first character) is capitalized)
                              NC = No obvious capitalization (will show this if all uppercase or all lowercase)

        Characters 07 & 08:   MC = Mixed cases (letters uppercase that are not the first letter of a word)
                              LC = Lower cases (letters that are not first letter of a word are all lower case)

        Characters 09 - 12    Four-digit number that is the length of the string (including spaces)

    */
    CREATE FUNCTION [dbo].[LFS_STRING_CAP_CHECK] (@check_value VARCHAR(4000)) Returns CHAR(12)
    AS BEGIN

        DECLARE @char_num INT = 1, @return_code CHAR(12)
        DECLARE @upp_exists CHAR(1) = 'N', @low_exists CHAR(1) = 'N'
        DECLARE @first_char_cap CHAR(1) = 'N', @first_char_each_cap CHAR(1) = 'N'
                        
        DECLARE @char_table TABLE ([id] INT IDENTITY(1,1), 
                                   [char_val] CHAR(1), 
                                   [char_type] CHAR(1),
                                   [char_ascii] INT,
                                   [prev_val] CHAR(1),
                                   [prev_type] CHAR(1),
                                   [prev_ascii] int)

        SELECT @check_value = ISNULL(@check_value,'')
        
        WHILE @char_num <= LEN(@check_value) BEGIN

            INSERT INTO @char_table ([char_val], [char_type], [char_ascii], [prev_val], [prev_type], [prev_ascii])
            SELECT  SUBSTRING(@check_value,@char_num,1),
                    CASE WHEN ASCII(SUBSTRING(@check_value,@char_num,1)) BETWEEN 65 AND 90 THEN 'U'
                         WHEN ASCII(SUBSTRING(@check_value,@char_num,1)) BETWEEN 97 AND 122 THEN 'L'
                         WHEN ASCII(SUBSTRING(@check_value,@char_num,1)) = 32 THEN 'S'
                         ELSE 'N' END,
                    ASCII(SUBSTRING(@check_value,@char_num,1)),
                    CASE WHEN @char_num = 1 THEN ''
                         ELSE SUBSTRING(@check_value,(@char_num - 1),1) END,
                    CASE WHEN @char_num = 1 THEN ''
                         WHEN ASCII(SUBSTRING(@check_value,(@char_num - 1),1)) BETWEEN 65 AND 90 THEN 'U'
                         WHEN ASCII(SUBSTRING(@check_value,(@char_num - 1),1)) BETWEEN 97 AND 122 THEN 'L'
                         WHEN ASCII(SUBSTRING(@check_value,(@char_num - 1),1)) = 32 THEN 'S'
                         ELSE 'N' END,
                    CASE WHEN @char_num = 1 THEN 0
                         ELSE ASCII(SUBSTRING(@check_value,(@char_num - 1),1)) END
            
            SELECT @char_num += 1
    
        END    

        /*  Delete anything that is not a letter  */

            DELETE FROM @char_table WHERE [char_type] = 'N'


        IF EXISTS(SELECT * FROM @char_table WHERE char_type = 'U')                      
                    SELECT @upp_exists = 'Y'
    
        IF EXISTS(SELECT * FROM @char_table WHERE char_type = 'L')                      
                    SELECT @low_exists = 'Y'

        IF EXISTS (SELECT * FROM @char_table WHERE [id] = 1 AND char_type = 'Y')        
                    SELECT @first_char_cap = 'Y'

        IF NOT EXISTS (SELECT * FROM @char_table WHERE  ([id] = 1 or [prev_ascii] = 32) AND [char_type] = 'L')
                    SELECT @first_char_each_cap = 'Y'

        IF @upp_exists = 'Y' AND @low_exists = 'Y'
                    SELECT @return_code = 'MXCS'
        ELSE IF @upp_exists = 'Y' AND @low_exists = 'N'
                    SELECT @return_code = 'UPCS'
        ELSE IF @upp_exists = 'N' AND @low_exists = 'Y'
                    SELECT @return_code = 'LWCS'

        IF @upp_exists = 'Y' and @low_exists = 'Y' AND @first_char_each_cap = 'Y' 
                    SELECT @return_code = RTRIM(@return_code) + 'WC'
        ELSE IF @upp_exists = 'Y' and @low_exists = 'Y' AND @first_char_cap = 'Y' 
                    SELECT @return_code = RTRIM(@return_code) + 'SC'
        ELSE 
                    SELECT @return_code = RTRIM(@return_code) + 'NC'

        IF @upp_exists = 'Y' and @low_exists = 'N'
                    SELECT @return_code = RTRIM(@return_code) + 'MC'
        ELSE IF EXISTS (SELECT * FROM @char_table WHERE [id] <> 1 and [prev_ascii] <> 32 AND [char_type] = 'U')
                    SELECT @return_code = RTRIM(@return_code) + 'MC'
        ELSE
                    SELECT @return_code = RTRIM(@return_code) + 'LC'

        SELECT @return_code = RTRIM(@return_code) + FORMAT(LEN(@check_value),'0000')

        FINISHED:

            IF ISNULL(@return_code,'') = '' SELECT @return_code = 'XXXXXXXX0000'

            RETURN @return_code

END
GO

GRANT EXECUTE ON [dbo].[LFS_STRING_CAP_CHECK] TO impusers
GO

SELECT [dbo].[LFS_STRING_CAP_CHECK] ('This Is A Test')
SELECT [dbo].[LFS_STRING_CAP_CHECK] ('THIS IS A TEST')
SELECT [dbo].[LFS_STRING_CAP_CHECK] ('this is a test!')
SELECT [dbo].[LFS_STRING_CAP_CHECK] ('ThIs Is A TeSt')
SELECT [dbo].[LFS_STRING_CAP_CHECK] ('')
SELECT [dbo].[LFS_STRING_CAP_CHECK] (NULL)