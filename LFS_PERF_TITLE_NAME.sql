

        ALTER FUNCTION [dbo].[LFS_PERF_TITLE_NAME] (@perf_no AS INT) RETURNS VARCHAR(30)
        AS BEGIN
            
            DECLARE @title_name AS VARCHAR(30) = ''

            SELECT @perf_no = ISNULL(@perf_no,0)

            IF @perf_no >0
                SELECT @title_name = ISNULL(inv.[description],'')
                FROM dbo.T_PERF AS prf (NOLOCK)
                     INNER JOIN dbo.T_PROD_SEASON AS sea (NOLOCK) ON sea.[prod_season_no] = prf.[prod_season_no]
                     INNER JOIN dbo.T_PRODUCTION AS pro (NOLOCK) ON pro.[prod_no] = sea.[prod_no]
                     INNER JOIN dbo.T_TITLE AS ttl (NOLOCK) ON ttl.[title_no] = pro.[title_no]
                     INNER JOIN dbo.T_INVENTORY AS inv (NOLOCK) ON inv.[inv_no] = ttl.[title_no]
                WHERE prf.perf_no = @perf_no


            RETURN @title_name

        END
        GO