USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LFS_IsLeapYear]') AND type IN (N'FN', N'IF', N'TF', N'FS', N'FT'))
    DROP FUNCTION [dbo].[LFS_IsLeapYear] 
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[LFS_IsLeapYear] (@check_year INT) RETURNS CHAR(1)
AS BEGIN

    DECLARE @return_val CHAR(1) = 'N';
    
    DECLARE @check_dt DATETIME = CONVERT(DATETIME,CAST(@check_year AS VARCHAR(4)) + '/01/01')

    SELECT @return_val = CASE  DATEPART(mm, DATEADD(dd, 1, CAST((CAST(DATEPART(YEAR,@check_dt) as varchar(4)) + '0228') as DATETIME)))
                          WHEN 2 THEN 'Y' ELSE  'N' END;

     
     RETURN @return_val

END
GO

GRANT EXECUTE ON [dbo].[LFS_IsLeapYear] TO [ImpUsers] AS [dbo]
GO

/*
--For Testing

SELECT [dbo].[LFS_IsLeapYear](2016) AS '2016', 
       [dbo].[LFS_IsLeapYear](2017) AS '2017', 
       [dbo].[LFS_IsLeapYear](2018) AS '2018', 
       [dbo].[LFS_IsLeapYear](2019) AS '2019', 
       [dbo].[LFS_IsLeapYear](2020) AS '2020', 
       [dbo].[LFS_IsLeapYear](2021) AS '2021' 
*/