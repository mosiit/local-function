USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[LFS_BOARD_GetProspectManagerInitials](@customer_no INT)
RETURNS VARCHAR(55) AS

BEGIN 

	DECLARE @ProspectManager VARCHAR(55) = ''

	SELECT @ProspectManager = SUBSTRING(key_value, 1,3)
	FROM dbo.TX_CUST_KEYWORD
	WHERE keyword_no = 555
		AND customer_no IN (SELECT expanded_customer_no FROM dbo.V_CUSTOMER_WITH_PRIMARY_GROUP WHERE customer_no = @customer_no)

	RETURN @ProspectManager

END 
GO

