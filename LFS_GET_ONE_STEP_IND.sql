USE impresario
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LFS_GET_ONE_STEP_IND]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
    DROP FUNCTION [dbo].[LFS_GET_ONE_STEP_IND]
GO

/*  pass it a subline item number and it will return a Y if the product on that subline item 
    is a membership and they have been enrolled in the One-Step program.   */

CREATE FUNCTION [dbo].[LFS_GET_ONE_STEP_IND] (@sli_no int) Returns char(1)
AS BEGIN

    DECLARE @return_ind char(1), @create_dt datetime
    DECLARE @order_no int, @customer_no int, @perf_no int, @zone_no int, @title_name varchar(30)
        
    /*  Check subline item number and make sure it's a number greater than zero  */

        SELECT @sli_No = Isnull(@sli_no, 0)
        IF @sli_no <= 0 GOTO FINISHED
        
    /*  Get perofrmance information - zeros not valid  */

        SELECT @perf_no = perf_no, @zone_no = zone_no, @order_no = [order_no], @create_dt = [create_dt] FROM [dbo].[T_SUB_LINEITEM] WHERE [sli_no] = @sli_No
        SELECT @perf_no = IsNull(@perf_no, 0), @zone_no = IsNull(@zone_no, 0), @order_no = IsNull(@order_no, 0)
        IF @perf_no <= 0 or @zone_no <= 0 or @order_no <= 0 GOTO FINISHED

    /*  Get customer number for the order the subline item is attached to - Zero not valid */

        SELECT @customer_no = [customer_no] FROM [dbo].[T_ORDER] WHERE [order_no] = @order_no
        SELECT @customer_no = IsNull(@customer_no, 0)
        IF @customer_no <= 0 GOTO FINISHED
    
    /* Get title name for the performance - Membership is the only valid valie  */

        SELECT @title_name = max([title_name]) FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] WHERE [performance_no] = @perf_no and [performance_zone] = @zone_no
        SELECT @title_name = IsNull(@title_name, '')
        IF @title_name <> 'Membership' GOTO FINISHED

    /* If it is a membership, look for CSI created on the same date for this customer with an activity type of 26 (Enroll in One-Step)  */

        IF exists (SELECT * FROM [dbo].[T_CUST_ACTIVITY] (NOLOCK) WHERE convert(char(10),[create_dt],111) = convert(char(10),@create_dt,111) and [activity_type] = 26 and [customer_no] = @customer_no)
            SELECT @return_ind = 'Y'
        ELSE 
            SELECT @return_ind = 'N'

    FINISHED:

    RETURN IsNull(@return_ind, 'N')

END
GO

GRANT EXECUTE ON [dbo].[LFS_GET_ONE_STEP_IND] TO impusers
GO







