USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LFS_GET_ORDER_VISIT_COUNT_BY_EXH]') AND type IN (N'FN', N'IF', N'TF', N'FS', N'FT'))
    DROP FUNCTION [dbo].[LFS_GET_ORDER_VISIT_COUNT_BY_EXH]
GO

/*  pass it a, order numner and it will the visit count for that particular order.  Based on sales, not scanned attendance.   */

CREATE FUNCTION [dbo].[LFS_GET_ORDER_VISIT_COUNT_BY_EXH] (@order_no int) Returns int
AS BEGIN

    /*  Function Parameters  */

        DECLARE @return_count INT = 0
        DECLARE @mos_buyout_no INT = (SELECT ISNULL([id],0) FROM [dbo].[TR_MOS] WHERE [description] = 'Buyouts')
        DECLARE @exhibit_hall_perf_no TABLE (perf_no INT)
        DECLARE @visit_count_raw_data TABLE ([attend_type] varchar(30), [order_no] int, [mode_of_sale] INT, [perf_no] int, [zone_no] int, 
                                             [prod_name] varchar(30), [sale_total] int, [scan_admission_total] int)

    /*  Check Parameters  */

        IF ISNULL(@order_no,0) = 0 GOTO FINISHED
    
    /*  Retrieve the Visit Count data Using the Exhibit Hall Count  */
    
        SELECT @return_count = COUNT(s.[sli_no]) 
        FROM dbo.T_SUB_LINEITEM AS s (NOLOCK)
             LEFT OUTER JOIN dbo.LV_PRODUCTION_ELEMENTS_PERFORMANCE AS p (NOLOCK) ON p.performance_no = s.perf_no AND p.performance_zone = s.zone_no
        WHERE s.order_no = @order_no AND (s.sli_status IN (3,12) OR s.sli_status = 2 AND p.performance_dt > GETDATE())
          AND p.title_name = 'Exhibit Halls' AND p.production_name <> 'Exhibit Halls Special' AND p.production_name NOT LIKE '%buyout%'

    FINISHED:

    RETURN IsNull(@return_count, 0)

END
GO

GRANT EXECUTE ON [dbo].[LFS_GET_ORDER_VISIT_COUNT_BY_EXH] TO [ImpUsers] AS [dbo]
GO

   