USE impresario
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LFS_GET_PERFORMANCE_PRICE]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
    DROP FUNCTION [dbo].[LFS_GET_PERFORMANCE_PRICE]
GO

/*  pass it a performance number, a zone number, a price type number and the date and time the sale was or is being made and 
    it will return the price for that price type in that performance on that date, at that time.   */

CREATE FUNCTION [dbo].[LFS_GET_PERFORMANCE_PRICE] (@perf_no int, @zone_no int, @price_type int,  @sale_date datetime) Returns decimal(18,2)
AS BEGIN

    DECLARE @return_price decimal (18,2)

    SELECT @return_price = IsNull(pev.[price], ppr.[start_price])
    FROM [dbo].[T_PERF_PRICE_TYPE] as ppt (NOLOCK)
         LEFT OUTER JOIN [dbo].[T_PERF_PRICE] as ppr (NOLOCK) ON ppr.[perf_price_type] = ppt.[id] and ppr.[start_enabled] = 'Y'
         LEFT OUTER JOIN [dbo].[TR_PRICE_TYPE] as pty (NOLOCK) ON pty.[id] = ppt.[price_type]
         LEFT OUTER JOIN T_PRICE_EVENT as pev (NOLOCK) ON pev.[perf_price_no] = ppr.[id] and pev.[enabled] = 'Y' and @sale_date > pev.[start_dt] 
    WHERE ppt.[perf_no] = @perf_no and ppr.[zone_no] = @zone_no and ppt.[price_type] = @price_type

    RETURN IsNull(@return_price, -1.00)
END
GO

GRANT EXECUTE ON [dbo].[LFS_GET_PERFORMANCE_PRICE] TO impusers
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LFS_GET_CURRENT_PERFORMANCE_PRICE]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
    DROP FUNCTION [dbo].[LFS_GET_CURRENT_PERFORMANCE_PRICE]
GO

/*  pass it a performance number, a zone number, a price type number and the date and time the sale was or is being made and 
    it will return the price for that price type in that performance on that date, at that time.   */

CREATE FUNCTION [dbo].[LFS_GET_CURRENT_PERFORMANCE_PRICE] (@perf_no int, @zone_no int, @price_type int) Returns decimal(18,2)
AS BEGIN

    DECLARE @return_price decimal (18,2)

    SELECT @return_price = IsNull(pev.[price], ppr.[start_price])
    FROM [dbo].[T_PERF_PRICE_TYPE] as ppt (NOLOCK)
         LEFT OUTER JOIN [dbo].[T_PERF_PRICE] as ppr (NOLOCK) ON ppr.[perf_price_type] = ppt.[id] and ppr.[start_enabled] = 'Y'
         LEFT OUTER JOIN [dbo].[TR_PRICE_TYPE] as pty (NOLOCK) ON pty.[id] = ppt.[price_type]
         LEFT OUTER JOIN T_PRICE_EVENT as pev (NOLOCK) ON pev.[perf_price_no] = ppr.[id] and pev.[enabled] = 'Y' and getdate() > pev.[start_dt] 
    WHERE ppt.[perf_no] = @perf_no and ppr.[zone_no] = @zone_no and ppt.[price_type] = @price_type

    RETURN IsNull(@return_price, -1.00)
END
GO

GRANT EXECUTE ON [dbo].[LFS_GET_CURRENT_PERFORMANCE_PRICE] TO impusers
GO



--SELECT performance_no, performance_zone FROM LV_PRODUCTION_ELEMENTS_PERFORMANCE WHERE performance_date = '2016/07/06' and title_name = 'Exhibit Halls'
--SELECT [dbo].[LFS_GET_PERFORMANCE_PRICE] (5558, 236, 3, '7-4-2016 09:00:00')
--SELECT [dbo].[LFS_GET_PERFORMANCE_PRICE] (5558, 236, 3, '7-5-2016 09:00:00')
--SELECT [dbo].[LFS_GET_CURRENT_PERFORMANCE_PRICE] (5558, 236, 3)