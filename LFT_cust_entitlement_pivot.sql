USE [impresario]
GO

/****** Object:  UserDefinedFunction [dbo].[LFT_cust_entitlement_pivot]    Script Date: 3/11/2021 8:03:21 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Jonathan Smillie, Tessitura Extended Services
-- Create date: April 22, 2020 (most recent deployment) 
-- Description:	Modified LFT_CUST_ENTTTLEMENT_PIVOT to add evaluation of entitlement initiation and expiration dates against current date 
-- 
-- Modified:	Heather Sheridan, 2/8/2021
-- Added condition to check gifted pass dates when the customer # is zero.  Service Now ticket SCTASK0001993.
-- =============================================
ALTER FUNCTION [dbo].[LFT_cust_entitlement_pivot]
(
    -- Add the parameters for the function here
    @customer_no INT
)
RETURNS TABLE
AS
RETURN
(
    SELECT DISTINCT
           a.customer_no,
           b.entitlement_no,
           b.reset_type,
           b.ent_tkw_id,
           b.entitlement_desc,
           SUM(a.num_ent) num_ent,
           MAX(sub.num_used) num_used,
           b.is_adhoc
    FROM LTX_CUST_ENTITLEMENT a
    JOIN LTR_ENTITLEMENT b ON a.entitlement_no = b.entitlement_no
    LEFT JOIN
    (
        SELECT a.entitlement_no,
               a.customer_no,
               SUM(a.num_used) num_used,
               b.reset_type,
               b.ent_tkw_id,
               b.is_adhoc
        FROM LTX_CUST_ORDER_ENTITLEMENT a
        JOIN LTR_ENTITLEMENT b ON a.entitlement_no = b.entitlement_no
                                  AND EXISTS
                                      (
                                          SELECT 1
                                          FROM LTX_CUST_ENTITLEMENT x
                                          WHERE x.entitlement_no = a.entitlement_no
                                                AND GETDATE()
                                                BETWEEN COALESCE(x.init_dt, '1/1/1900') AND COALESCE(
                                                                                                        x.expr_dt,
                                                                                                        '1/1/2999'
                                                                                                    )
                                                AND x.customer_no = @customer_no
                                      )
        WHERE customer_no = @customer_no
              AND (b.reset_type = 'M')
              AND
              (
                  COALESCE(a.cust_memb_no, 0) = 0
				  -- H. Sheridan, 2/8/2021 - Service Now ticket SCTASK0001993
                  AND EXISTS
        (
            SELECT 1
            FROM LTX_CUST_ENTITLEMENT ce
            JOIN LT_ENTITLEMENT_GIFT eg ON ce.gift_no = eg.id_key
            WHERE ce.customer_no = @customer_no
                  AND ce.gift_no IS NOT NULL
                  AND GETDATE()
                  BETWEEN COALESCE(eg.init_dt, '1/1/1900') AND COALESCE(eg.expr_dt, '1/1/2999')
        )
                  OR a.cust_memb_no IN
                     (
                         SELECT TX_CUST_MEMBERSHIP.cust_memb_no
                         FROM dbo.TX_CUST_MEMBERSHIP
                         WHERE current_status IN ( 2, 3 )
                               AND declined_ind = 'N'
                     )
              )
              AND b.is_adhoc = 'N'
              AND COALESCE(b.inactive, 'N') = 'N'
        GROUP BY a.entitlement_no,
                 a.customer_no,
                 b.reset_type,
                 b.ent_tkw_id,
                 b.is_adhoc
    ) sub ON sub.customer_no = a.customer_no
             AND sub.entitlement_no = a.entitlement_no
    WHERE a.customer_no = @customer_no
          AND b.is_adhoc = 'N'
          AND
          (
              COALESCE(a.cust_memb_no, 0) = 0
			  -- H. Sheridan, 2/8/2021 - Service Now ticket SCTASK0001993
              AND EXISTS
    (
        SELECT 1
        FROM LTX_CUST_ENTITLEMENT ce
        JOIN LT_ENTITLEMENT_GIFT eg ON ce.gift_no = eg.id_key
        WHERE ce.customer_no = @customer_no
              AND ce.gift_no IS NOT NULL
              AND GETDATE()
              BETWEEN COALESCE(eg.init_dt, '1/1/1900') AND COALESCE(eg.expr_dt, '1/1/2999')
    )
              OR a.cust_memb_no IN
                 (
                     SELECT TX_CUST_MEMBERSHIP.cust_memb_no
                     FROM dbo.TX_CUST_MEMBERSHIP
                     WHERE current_status IN ( 2, 3 )
                           AND declined_ind = 'N'
                           AND
                           (
                               a.expr_dt IS NULL
                               OR GETDATE()
                           BETWEEN a.init_dt AND a.expr_dt
                           )
                 )
          )
    GROUP BY a.customer_no,
             b.entitlement_no,
             sub.num_used,
             b.reset_type,
             b.ent_tkw_id,
             b.is_adhoc,
             b.entitlement_desc
    UNION ALL
    SELECT --cust_memb_no,
        customer_no,
        entitlement_no,
        reset_type,
        ent_tkw_id,
        entitlement_desc,
        SUM(num_ent),
        SUM(num_used),
        is_adhoc
    FROM
    (
        SELECT DISTINCT
               COALESCE(a.cust_memb_no, 0) cust_memb_no,
               a.customer_no,
               a.entitlement_no,
               b.reset_type,
               b.ent_tkw_id,
               b.entitlement_desc,
               a.num_ent,
               b.is_adhoc,
               COALESCE(SUM(c.num_used) OVER (PARTITION BY a.customer_no, a.entitlement_no, a.id_key), 0) AS num_used,
               a.id_key
        FROM dbo.LTX_CUST_ENTITLEMENT AS a
        INNER JOIN dbo.LTR_ENTITLEMENT AS b ON a.entitlement_no = b.entitlement_no
                                               AND a.customer_no = @customer_no
                                               AND b.is_adhoc = 'Y'
                                               AND GETDATE()
                                               BETWEEN COALESCE(a.init_dt, '1/1/1900') AND COALESCE(
                                                                                                       a.expr_dt,
                                                                                                       '1/1/2999'
                                                                                                   )
        LEFT JOIN LTX_CUST_ORDER_ENTITLEMENT c ON a.customer_no = c.customer_no
                                                  AND a.entitlement_no = c.entitlement_no
                                                  AND a.id_key = c.ltx_cust_entitlement_id
        WHERE
            --	coalesce(a.inactive,'N')='N'and 
            COALESCE(b.inactive, 'N') = 'N'
    ) sub
    GROUP BY sub.customer_no,
             sub.entitlement_no,
             sub.entitlement_desc,
             sub.ent_tkw_id,
             sub.reset_type,
             sub.is_adhoc,
             sub.cust_memb_no
);









GO


