USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LFS_FISCAL_OFFSET]') AND type IN (N'FN', N'IF', N'TF', N'FS', N'FT'))
    DROP FUNCTION [dbo].[LFS_FISCAL_OFFSET]
GO

/*  LFS_FISCAL_OFFSET
    Returns a Number of years offset between current date and the contribution date if the month and day of the contribution date 
    is on or before the month and day of the current year.  
*/
CREATE FUNCTION [dbo].[LFS_FISCAL_OFFSET] (@cont_dt DATETIME) RETURNS VARCHAR(15)
AS BEGIN

    --Variables
    DECLARE @current_fy INT = 0, @contribution_fy INT = 0
    DECLARE @offset_count INT = 0, @offset_date DATE = NULL, @offset_return2 VARCHAR(15) = ''

    --If Null is passed, return OTHER
    IF @cont_dt IS NULL GOTO FINISHED
        
    --Get Current Fiscal Year
    SELECT @current_fy = [fyear] 
           FROM [dbo].[TR_BATCH_PERIOD]
           WHERE CAST(GETDATE() AS DATE) BETWEEN CAST([start_dt] AS DATE) and CAST([end_dt] AS DATE)

    --Get Fiscal Year of the Contribution
    SELECT @contribution_fy = [fyear] 
           FROM [dbo].[TR_BATCH_PERIOD]
           WHERE CAST(@cont_dt AS DATE) BETWEEN CAST([start_dt] AS DATE) AND CAST([end_dt] AS DATE)
    
    --The Offset count (a negative number) is the difference when the current fiscal year is subtracted from the contribution fiscal year
    SELECT @offset_count = (@contribution_fy - @current_fy)

    /*  Original Code -  Checks each year individually and only goes back 8 years  - Left here commented as reference but not used  
        DECLARE @offset_return1 VARCHAR(15) = ''
        SELECT @offset_return1 = CASE WHEN @contribution_fy = @current_fy THEN 'YTD' 
                                      WHEN @contribution_fy - @current_fy = -1 AND CAST(@cont_dt AS DATE) <= CAST (DATEADD(Year,-1,GETDATE()) AS DATE) THEN 'YTDMinus1' 
                                      WHEN @contribution_fy - @current_fy = -2 AND CAST(@cont_dt AS DATE) <= CAST (DATEADD(Year,-2,GETDATE()) AS DATE) THEN 'YTDMinus2' 
                                      WHEN @contribution_fy - @current_fy = -3 AND CAST(@cont_dt AS DATE) <= CAST (DATEADD(Year,-3,GETDATE()) AS DATE) THEN 'YTDMinus3'
                                      WHEN @contribution_fy - @current_fy = -4 AND CAST(@cont_dt AS DATE) <= CAST (DATEADD(Year,-4,GETDATE()) AS DATE) THEN 'YTDMinus4'
                                      WHEN @contribution_fy - @current_fy = -5 AND CAST(@cont_dt AS DATE) <= CAST (DATEADD(Year,-5,GETDATE()) AS DATE) THEN 'YTDMinus5'
                                      WHEN @contribution_fy - @current_fy = -6 AND CAST(@cont_dt AS DATE) <= CAST (DATEADD(Year,-6,GETDATE()) AS DATE) THEN 'YTDMinus6'
                                      WHEN @contribution_fy - @current_fy = -7 AND CAST(@cont_dt AS DATE) <= CAST (DATEADD(Year,-7,GETDATE()) AS DATE) THEN 'YTDMinus7'
                                      WHEN @contribution_fy - @current_fy = -8 AND CAST(@cont_dt AS DATE) <= CAST (DATEADD(Year,-8,GETDATE()) AS DATE) THEN 'YTDMinus8'
                                      ELSE 'OTHER' END   */

        --The Offset date is the today's date (month and day) in the previous fiscal year
        SELECT @offset_date = DATEADD(YEAR,@offset_count,GETDATE())
        
        --If the contribution date <= the offset date then the contribution date is considered part of YTD (return the offset of how many years back)
        --Otherwise, it's not (return OTHER)
        SELECT @offset_return2 = CASE WHEN @contribution_fy = @current_fy THEN 'YTD' 
                                      WHEN @cont_dt <= @offset_date THEN 'YTDMinus' + CAST((@offset_count * -1) AS VARCHAR(5))
                                      ELSE 'OTHER' END

    FINISHED:

        IF ISNULL(@offset_return2,'') = '' SELECT @offset_return2 = 'OTHER'

        RETURN @offset_return2

END
GO

GRANT EXECUTE ON [dbo].[LFS_FISCAL_OFFSET] TO [ImpUsers], [tessitura_app]
GO


SELECT [dbo].[LFS_FISCAL_OFFSET] ('4-11-2008')
SELECT [dbo].[LFS_FISCAL_OFFSET] ('4-12-2019')


--DECLARE @results TABLE (cont_dt datetime, contribution_fy int, current_fy int, offset_count1 int, offset_return1 varchar(15), offset_return2 VARCHAR(15))
--INSERT INTO @results ([cont_dt],[contribution_fy],[current_fy],[offset_count1],[offset_return1],[offset_return2])                
--SELECT @cont_dt, @contribution_fy, @current_fy, @offset_count1, @offset_return1, @offset_return2
--SELECT @cont_dt = DATEADD(DAY,1,@cont_dt)


