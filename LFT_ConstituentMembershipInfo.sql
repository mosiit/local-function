USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LFT_ConstituentMembershipInfo]') AND type IN (N'FN', N'IF', N'TF', N'FS', N'FT'))
    DROP FUNCTION [dbo].[LFT_ConstituentMembershipInfo]
GO

/*  LFT_ConstituentMembershipInfo
    Returns the membership information for a customer
 
        
    There are three values passed to the function
       @customer_no = the customer to check
          @memb_org = a specific membership organization If 0, check all membership organizations
          @check_dt = Were they or will they be a valid member on this date?
      
    NOTE: Only memberships with a status of 1 (inactive), 2 (active) or 3 (pending) are looked at.
          Memberships with any other status like suspended, deactivated or merged will be ignored.
          
    Example:  SELECT [dbo].[LFS_ActiveMemberNumberOnDate] (67226, 4, '7-1-2018')  */

CREATE FUNCTION [dbo].[LFT_ConstituentMembershipInfo] (@customer_no INT = 0, @memb_org INT = 0, @check_dt DATETIME = NULL, @ignore_pending CHAR(1)) 
                                                       RETURNS @mem_table TABLE ([customer_no] INT NOT NULL DEFAULT (0),
                                                                                 [display_name] VARCHAR(150) NOT NULL DEFAULT (''),
                                                                                 [mem_no] INT NOT NULL DEFAULT (0), 
                                                                                 [mem_status] INT NOT NULL DEFAULT (0), 
                                                                                 [mem_status_name] VARCHAR(30) NOT NULL DEFAULT (''), 
                                                                                 [mem_org] INT NOT NULL DEFAULT (0), 
                                                                                 [mem_org_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                                                                 [mem_level] VARCHAR(10) NOT NULL DEFAULT (''),
                                                                                 [mem_level_name] VARCHAR(30) NOT NULL DEFAULT (''),
                                                                                 [mem_create] DATETIME NULL, 
                                                                                 [mem_init] DATETIME NULL, 
                                                                                 [mem_expire] DATETIME NULL)
AS BEGIN

    /*  Check Arguments  */

        SELECT @customer_no = ISNULL(@customer_no,0)

        SELECT @memb_org = ISNULL(@memb_org, 0)

        SELECT @ignore_pending = ISNULL(@ignore_pending,'N')

         IF @check_dt IS NOT NULL SELECT @check_dt = CAST(@check_dt AS DATE)
  
     /*  Get all membership records for this customer - limit to specific organization if one was passed in
         Ignore records that aren't pending, active, or inactive - Remove times from all date/time values  */

        INSERT INTO @mem_table ([customer_no], [display_name], [mem_no], [mem_status], [mem_status_name], [mem_org],
                                [mem_org_name], [mem_level], [mem_level_name], [mem_create], [mem_init], [mem_expire])
            SELECT xcm.[customer_no],
                   nam.[display_name],
                   xcm.[cust_memb_no], 
                   xcm.[current_status], 
                   sta.[description],
                   xcm.[memb_org_no], 
                   org.[description],
                   xcm.[memb_level],
                   lev.[description],
                   xcm.[create_dt], 
                   CAST(xcm.[init_dt] AS DATE), 
                   CONVERT(CHAR(10),xcm.[expr_dt],111) + ' 23:59:59.957'
            FROM [dbo].[TX_CUST_MEMBERSHIP]  AS xcm
                 INNER JOIN [dbo].[T_MEMB_ORG] AS org ON org.[memb_org_no] = xcm.[memb_org_no]
                 INNER JOIN [dbo].[T_MEMB_LEVEL] AS lev ON lev.[memb_org_no] = xcm.[memb_org_no] and lev.[memb_level] = xcm.[memb_level]
                 INNER JOIN [dbo].[TR_CURRENT_STATUS] AS sta ON sta.[id] = xcm.[current_status]
                 INNER JOIN [dbo].[FT_CONSTITUENT_DISPLAY_NAME]() AS nam ON nam.[customer_no] = xcm.[customer_no]
            WHERE (@customer_no = 0 OR xcm.[customer_no] = @customer_no)
              AND (@memb_org = 0 OR xcm.[memb_org_no] = @memb_org)
              AND (@check_dt IS NULL OR @check_dt  BETWEEN xcm.[init_dt] AND xcm.[expr_dt])
              AND (xcm.[current_status] IN (1, 2) OR (@ignore_pending = 'N' and xcm.[current_status] = 3))   --1=Inactive/2=Active/3=Pending
            ORDER BY xcm.[current_status] DESC, xcm.[expr_dt] DESC  
        
    FINISHED:

        /*  Return final value  */

            RETURN;

END
GO


GRANT SELECT ON [dbo].[LFT_ConstituentMembershipInfo] TO ImpUsers
GRANT SELECT ON [dbo].[LFT_ConstituentMembershipInfo] TO tessitura_app
GO

SELECT * FROM [dbo].[LFT_ConstituentMembershipInfo](67226,0,NULL,'Y')
