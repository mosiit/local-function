USE [impresario]
GO
/****** Object:  UserDefinedFunction [dbo].[LFT_CUSTOMER_ENTITLEMENT_COUNT]    Script Date: 11/27/2018 7:14:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
--select * from dbo.[LFT_CUSTOMER_ENTITLEMENT_COUNT_new]( 3735554,1503143)
-- =============================================
ALTER FUNCTION [dbo].[LFT_CUSTOMER_ENTITLEMENT_COUNT]
(	
	-- Add the parameters for the function here
	@customer_no int,
	@order_no int	
)
RETURNS TABLE 
AS
RETURN 
(
 
		
select * from (
SELECT DISTINCT
	a.cust_memb_no, 
    a.customer_no,
    a.entitlement_no,
    b.reset_type,
    b.ent_tkw_id,
    b.entitlement_desc,
    a.num_ent,
    SUM(c.num_used) OVER (PARTITION BY a.customer_no,c.entitlement_no, c.cust_memb_no) AS num_used,
	b.is_adhoc,
	a.id_key
FROM dbo.LTX_CUST_ENTITLEMENT AS a
INNER JOIN dbo.LTR_ENTITLEMENT AS b
    ON a.entitlement_no = b.entitlement_no
LEFT OUTER JOIN dbo.LTX_CUST_ORDER_ENTITLEMENT AS c
    ON a.customer_no = c.customer_no
       AND a.cust_memb_no = c.cust_memb_no
       AND a.entitlement_no = c.entitlement_no
WHERE (a.num_ent > 0)
      AND (b.reset_type = 'M')
      AND (coalesce(a.cust_memb_no,0)=0 or  a.cust_memb_no IN (
                                SELECT TX_CUST_MEMBERSHIP.cust_memb_no FROM dbo.TX_CUST_MEMBERSHIP WHERE current_status IN (2, 3) AND declined_ind = 'N'
                            ))
	and b.is_adhoc='N'
	and a.customer_no=@customer_no
	and exists (select 1		
			from T_ORDER (nolock) xo
			join T_SUB_LINEITEM  (nolock) so on xo.order_no = so.order_no
			join T_PERF xp on xp.perf_no = so.perf_no
			JOIN dbo.TR_PRICE_TYPE xpt ON xpt.id = so.price_type
			join LV_ENTITLEMENT_INV_TKW_LIST xt on xt.perf_no = so.perf_no
			join LTR_ENTITLEMENT xe on xe.ent_tkw_id = xt.tkw
				and xpt.price_type_group = xe.ent_price_type_group
			join dbo.LTX_CUST_ENTITLEMENT xce on xce.entitlement_no = xe.entitlement_no
				and xce.customer_no = a.customer_no
				and a.id_key = xce.id_key
			where xo.order_no = @order_no
			AND (
		CASE WHEN xe.date_to_compare = 'O' 
			THEN xo.order_dt
			ELSE xp.perf_dt
			END 
		BETWEEN CAST(CONVERT(varchar(10),xce.init_dt,101) as datetime) 
			AND CAST(CONVERT(varchar(10),xce.expr_dt,101) as datetime) + '23:59:59')
			)
	
union all
select distinct a.cust_memb_no,
		a.customer_no,
		a.entitlement_no,
		a.reset_type,
		a.ent_tkw_id,
		a.entitlement_desc,
		a.num_ent,
	SUM(c.num_used) OVER (PARTITION BY a.customer_no,a.entitlement_no,a.id_key) AS num_used,
	a.is_adhoc,
	a.id_key
from 
(SELECT
distinct 	coalesce(a.cust_memb_no, 0) cust_memb_no,
    a.customer_no,
    a.entitlement_no, 
	b.reset_type,
    b.ent_tkw_id,
    b.entitlement_desc,   
	   sum( a.num_ent )OVER (PARTITION BY a.customer_no,a.entitlement_no,a.id_key) AS num_ent,
	   b.is_adhoc,a.id_key
FROM dbo.LTX_CUST_ENTITLEMENT AS a
INNER JOIN dbo.LTR_ENTITLEMENT AS b
    ON a.entitlement_no = b.entitlement_no
	and a.customer_no= @customer_no
	and b.is_adhoc='Y'	
	and exists (select 1		
			from T_ORDER  (nolock) xo
			join T_SUB_LINEITEM  (nolock)so on xo.order_no = so.order_no
			join T_PERF xp on xp.perf_no = so.perf_no
			JOIN dbo.TR_PRICE_TYPE xpt ON xpt.id = so.price_type
			join LV_ENTITLEMENT_INV_TKW_LIST xt on xt.perf_no = so.perf_no
			join LTR_ENTITLEMENT xe on xe.ent_tkw_id = xt.tkw
				and xpt.price_type_group = xe.ent_price_type_group
			join dbo.LTX_CUST_ENTITLEMENT xce on xce.entitlement_no = xe.entitlement_no
				and xce.customer_no = a.customer_no
				and a.id_key = xce.id_key
			where xo.order_no = @order_no
			AND (
		CASE WHEN xe.date_to_compare = 'O' 
			THEN xo.order_dt
			ELSE xp.perf_dt
			END 
		BETWEEN CAST(CONVERT(varchar(10),xce.init_dt,101) as datetime) 
			AND CAST(CONVERT(varchar(10),xce.expr_dt,101) as datetime) + '23:59:59')
			)
	
	) a 
LEFT OUTER JOIN dbo.LTX_CUST_ORDER_ENTITLEMENT AS c
    ON a.customer_no = c.customer_no
     --  AND a.cust_memb_no = c.cust_memb_no
       AND a.entitlement_no = c.entitlement_no
	   and a.id_key=c.ltx_cust_entitlement_id
WHERE (a.num_ent > 0)
and a.customer_no=@customer_no) sub where   num_ent>coalesce(num_used,0)
)

