USE [impresario]
GO

/****** Object:  UserDefinedFunction [dbo].[LFS_ATTEND_BUDGET_DATA]    Script Date: 6/10/2019 11:38:45 AM ******/
DROP FUNCTION [dbo].[LFS_ATTEND_BUDGET_DATA]
GO

/****** Object:  UserDefinedFunction [dbo].[LFS_ATTEND_BUDGET_DATA]    Script Date: 6/10/2019 11:38:45 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[LFS_ATTEND_BUDGET_DATA] (@BudgetDate DATETIME)
RETURNS INT AS
BEGIN

	DECLARE @budnum INT

    SELECT @budnum = budget
    FROM   dbo.LTR_ATTENDANCE_BUDGET_DATA
    WHERE perf_dt = @BudgetDate
          AND title_group = 'Entire Museum'
          AND inactive = 'N';

    RETURN @budnum;

END;
GO


