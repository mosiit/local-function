USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LFS_GetMostRecentMembershipNumber]') AND type IN (N'FN', N'IF', N'TF', N'FS', N'FT'))
    DROP FUNCTION [dbo].[LFS_GetMostRecentMembershipNumber]
GO

/*  Does it's best to find the cust_memb_no of the most recent membership for a particular customer.
    Returns zero if no membership found.
    @customer_no = The customer to check
    @memb_org = a specific membership organization (4=Household).  If zero, check all membership organizations
    @active_only = Y/N - Only return number for a membership currently active.
                   If N, it will still look for an active membership first.  if it can't find one, it will
                   the number of the membership (if any) that has the most recent expiration date.  */

CREATE FUNCTION [dbo].[LFS_GetMostRecentMembershipNumber](@customer_no INT, @memb_org INT, @active_only CHAR(1)) RETURNS INT 
AS BEGIN 

    /*  Function Variables  */

        DECLARE @cust_memb_no INT = 0
    
        DECLARE @mem_table TABLE (mem_no INT, mem_status INT, mem_org INT, mem_expire DATETIME)

    /*  Get all membership records for this customer - limit to specific organization if one was passed in
        Ignore records that aren't pending, active, or inactive - Remove times from all date/time values  */

        INSERT INTO @mem_table ([mem_no], [mem_status], [mem_org], [mem_expire])
        SELECT [cust_memb_no], 
               [current_status], 
               [memb_org_no], 
               [expr_dt]
        FROM [dbo].[TX_CUST_MEMBERSHIP] 
        WHERE customer_no = @customer_no
          AND (@memb_org = 0 OR [memb_org_no] = @memb_org)
          AND [current_status] IN (1, 2, 3)

    /*  Delete non-active memberships if needed  */

        IF @active_only = 'Y'
            DELETE FROM @mem_table
            WHERE [mem_status] <> 2

    /*  If no memberships found for this customer, jump to the end  */

        IF NOT EXISTS (SELECT * FROM @mem_table) GOTO FINISHED
    
    /*  Look For the most recently entered membership (has the highest membership number) 
        where member status = active and expire date > now  */

        SELECT @cust_memb_no = ISNULL(MAX(mem_no),0) 
                          FROM @mem_table 
                          WHERE mem_status = 2 AND mem_expire > GETDATE()

    /*  If nothing found, look for the most recently entered membership (has the highest membership number(
        where expire date > now  */

        IF @cust_memb_no = 0
            SELECT @cust_memb_no = ISNULL(MAX([mem_no]),0) 
            FROM @mem_table 
            WHERE [mem_expire] > GETDATE()

    /*  If nothing found, look for the membership with the most recent expiration date  */

        IF @cust_memb_no = 0 
            SELECT @cust_memb_no = (SELECT TOP 1 [mem_no]
                                    FROM @mem_table
                                    ORDER BY [mem_expire] DESC)

    /*  Deal with a null value - Default to 0 */

        SELECT @cust_memb_no = ISNULL(@cust_memb_no,0)

    FINISHED:

        /*  Return final value  */

        RETURN @cust_memb_no

END
GO

GRANT EXECUTE ON [dbo].[LFS_GetMostRecentMembershipNumber] TO ImpUsers
GO

--SELECT [dbo].[LFS_GetMostRecentMembershipNumber] (67226,4,'N')






    --SELECT * FROM dbo.T_MEMB_ORG
    --SELECT * FROM dbo.TR_CURRENT_STATUS
