USE impresario
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LFS_GET_ONE_STEP_COUNT]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
    DROP FUNCTION [dbo].[LFS_GET_ONE_STEP_COUNT]
GO

/*  pass it a subline item number and it will return a 1 if the product on that subline item 
    is a membership and they have been enrolled in the One-Step program.   */

CREATE FUNCTION [dbo].[LFS_GET_ONE_STEP_COUNT] (@sli_no int) Returns int
AS BEGIN

    DECLARE @return_count INT = 0, @create_dt DATETIME, @has_constituency CHAR(1) = 'N'
    DECLARE @order_no INT = 0, @customer_no INT = 0, @perf_no INT = 0, @zone_no INT = 0, @title_name varchar(30) = ''
        
    /*  Check subline item number and make sure it's a number greater than zero  */

        SELECT @sli_No = Isnull(@sli_no, 0)
        IF @sli_no <= 0 GOTO FINISHED
        
    /*  Get perofrmance information - zeros not valid  */

        SELECT @perf_no = perf_no, @zone_no = zone_no, @order_no = [order_no], @create_dt = [create_dt] FROM [dbo].[T_SUB_LINEITEM] WHERE [sli_no] = @sli_No
        SELECT @perf_no = IsNull(@perf_no, 0), @zone_no = IsNull(@zone_no, 0), @order_no = IsNull(@order_no, 0)
        IF @perf_no <= 0 or @zone_no <= 0 or @order_no <= 0 GOTO FINISHED

    /*  Get customer number for the order the subline item is attached to - Zero not valid */

        SELECT @customer_no = [customer_no] FROM [dbo].[T_ORDER] WHERE [order_no] = @order_no
        SELECT @customer_no = IsNull(@customer_no, 0)
        IF @customer_no <= 0 GOTO FINISHED
    
    /* Get title name for the performance - Membership is the only valid valie  */

        SELECT @title_name = max([title_name]) FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] WHERE [performance_no] = @perf_no and [performance_zone] = @zone_no
        SELECT @title_name = IsNull(@title_name, '')
        IF @title_name <> 'Membership' GOTO FINISHED

    /*  Determine if customer already has the one-step constituency
        The constituency record must exist with a start date at least a month prior to the membership sale and have no end date 
                NOTE: THIS HAS BEEN ADDED ONLY TO THE ONE_STEP_COUNT FUNCTION AND NOT THE ONE_STEP_IND FUNCTION
                      AND CAN PRODUCE RECORDS WHERE THE ONE-STEP INDICATOR is Y BUT THE COUNT IS STILL 0.  */

        SELECT @create_dt = CONVERT(DATE,@create_dt)

        IF EXISTS (SELECT * FROM dbo.TX_CONST_CUST (NOLOCK)
                   WHERE customer_no = @customer_no AND constituency = 30
                     AND ISNULL([start_dt],CONVERT(DATE,GETDATE())) <= DATEADD(MONTH,-1,@create_dt) AND ([End_dt] IS NULL or ISNULL([end_dt],CONVERT(DATE,GETDATE() + 1)) > @create_dt))
            SELECT @has_constituency = 'Y'

    /* If it is a membership, look for CSI created on the same date for this customer with an activity type of 26 (Enroll in One-Step)  */

        IF exists (SELECT * FROM [dbo].[T_CUST_ACTIVITY] (NOLOCK) WHERE convert(char(10),[create_dt],111) = convert(char(10),@create_dt,111) and [activity_type] = 26 and [customer_no] = @customer_no) BEGIN

            IF @has_constituency = 'Y' SELECT @return_count = 0
            ELSE SELECT @return_count = 1
        
        END ELSE BEGIN

            SELECT @return_count = 0
  
        END

    FINISHED:

    RETURN IsNull(@return_count, 0)

END
GO

GRANT EXECUTE ON [dbo].[LFS_GET_ONE_STEP_COUNT] TO impusers
GO


SELECT [dbo].[LFS_GET_ONE_STEP_COUNT] (4288371)

