USE impresario
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[LFS_TPP_GetDomainList](@customer_no INT)
RETURNS VARCHAR(1000) AS

-- USED TO CREATE A LIST OF DOMAINS GIVEN A CUSTOMER_NO

BEGIN
	DECLARE @DomainList VARCHAR(1000)
	
	SELECT @DomainList = COALESCE(@DomainList + ',', '') + address
	FROM dbo.T_EADDRESS
	WHERE customer_no = @customer_no
		AND eaddress_type = 15 -- TPP Domain
		AND inactive = 'N'

	RETURN @DomainList
END
GO
