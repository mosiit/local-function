USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LFS_GET_NUMBER_OF_SHOWS]') AND type IN (N'FN', N'IF', N'TF', N'FS', N'FT'))
    DROP FUNCTION [dbo].[LFS_GET_NUMBER_OF_SHOWS]
GO

/*  Pass it a start date, number of days (including start date) and either a title number or one or two 
    productions numbers and it will count performances.  To count specific productions, @title_no needs to be 0.  
    If a title number is passed, @Prod1 and @prod2 are ignored.  */

CREATE FUNCTION [dbo].[LFS_GET_NUMBER_OF_SHOWS](@start_date DATE, @num_of_days INT, @title_no INT, @prod1 INT, @prod2 INT) RETURNS INT
AS BEGIN 

    DECLARE @end_date DATE, @prod1Shows INT = 0, @prod2Shows Int = 0
    DECLARE @show_count INT = 0

    IF ISNULL(@num_of_days, 0) <= 0 SELECT @num_of_days = 1

    SELECT @end_date = DATEADD(DAY, (@num_of_days - 1), @start_date)

    IF @title_no > 0 BEGIN

        SELECT @show_count = COUNT(DISTINCT performance_date + ' ' + performance_time) 
                                   FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE]
                                   WHERE CAST(performance_dt AS date) BETWEEN @start_date AND @end_date
                                     AND [title_no] = @title_no
                                  

    END ELSE BEGIN

        IF ISNULL(@prod1, 0) > 0 BEGIN

            SELECT @title_no = ISNULL([title_no], 0) FROM [dbo].[T_PRODUCTION] WHERE [prod_no] = @prod1           
            SELECT @prod1Shows = COUNT(DISTINCT performance_date + ' ' + performance_time) 
                                 FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE]
                                 WHERE CAST(performance_dt AS date) BETWEEN @start_date AND @end_date
                                   AND [title_no] = @title_no
                                   AND [production_no] = @prod1

        END

        IF ISNULL(@prod2, 0) > 0 BEGIN

            SELECT @title_no = ISNULL([title_no], 0) FROM [dbo].[T_PRODUCTION] WHERE [prod_no] = @prod2           
            SELECT @prod2Shows = COUNT(DISTINCT performance_date + ' ' + performance_time) 
                                 FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE]
                                 WHERE CAST(performance_dt AS date) BETWEEN @start_date AND @end_date
                                   AND [title_no] = @title_no
                                   AND [production_no] = @prod2

        END

        SELECT @show_count = (@prod1Shows + @prod2Shows)

    END

    FINISHED:



        RETURN @show_count

END
GO

GRANT EXECUTE ON [dbo].[LFS_GET_NUMBER_OF_SHOWS] TO [ImpUsers], [tessitura_app]
GO

--FOR TESTING
--SELECT [dbo].[LFS_GET_NUMBER_OF_SHOWS] ('2-15-2019', 42, 0, 52192, 0)
--SELECT [dbo].[LFS_GET_NUMBER_OF_SHOWS] ('10-15-2016', 42, 0, 11943, 30507)
--SELECT [dbo].[LFS_GET_NUMBER_OF_SHOWS] ('10-15-2016', 42, 161, 11943, 30507)


