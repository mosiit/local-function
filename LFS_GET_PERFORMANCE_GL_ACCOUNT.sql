USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LFS_GET_PERFORMANCE_GL_ACCOUNT]') AND type IN (N'FN', N'IF', N'TF', N'FS', N'FT'))
    DROP FUNCTION [dbo].[LFS_GET_PERFORMANCE_GL_ACCOUNT]
GO

/*  pass it a customer numner and it will return the customer's full name using the first, middle, and last name values  */

CREATE FUNCTION [dbo].[LFS_GET_PERFORMANCE_GL_ACCOUNT](@perf_no INT,  @price_type_no INT) RETURNS VARCHAR(50) 
AS BEGIN 

    /*  Function Variables  */

        DECLARE @return_gl VARCHAR(50) = ''

        DECLARE @default_gl VARCHAR(50) = ''
    
    /*  If null or zero is passed as the price type, determine a price type to use by 
        selecting the lowest price type number from that performance where the price
        is greater than zero.  */

        IF ISNULL(@price_type_no,0) <= 0
            SELECT @price_type_no = ISNULL(MIN(ppt.[price_type]),0)
                                    FROM [DBO].[T_PERF_PRICE] ppr
                                         LEFT OUTER JOIN [dbo].[T_PERF_PRICE_TYPE] AS ppt ON ppt.[id] = ppr.[perf_price_type]
                                    WHERE ppr.perf_no = @perf_no AND ppr.start_price > 0.0

    /*  Determine both the assigned GL number and the default GL number
        Most of the time, these are the same.  */

        SELECT @default_gl = MIN(gldf.[gl_account_no]),
               @return_gl = MIN(glnm.[gl_account_no])
        FROM [dbo].[T_PERF] AS prf (NOLOCK)
             INNER JOIN [dbo].[T_PERF_PRICE] AS pri (NOLOCK) ON pri.[perf_no] = prf.[perf_no] --AND pri.[zone_no] = prf.[performance_zone]
             INNER JOIN [dbo].[T_PERF_PRICE_TYPE] AS ptp (NOLOCK) ON ptp.[id] = pri.[perf_price_type]
             INNER JOIN [dbo].[T_PERF_PRICE_LAYER] AS lay (NOLOCK) ON lay.[id] = ptp.[perf_price_layer]
             LEFT OUTER JOIN [dbo].[TX_TEMPLATE_PRICE_TYPE] AS xtp (NOLOCK) ON xtp.[template] = lay.[template] AND xtp.[price_type] = ptp.[price_type]
             INNER JOIN [dbo].[TR_PRICE_TYPE] AS typ (NOLOCK) ON typ.[id] = ptp.[price_type]
             LEFT OUTER JOIN [dbo].[T_GL_ACCOUNT] AS gldf (NOLOCK) ON gldf.[id] = xtp.[gl_no]
             INNER JOIN [dbo].[T_GL_ACCOUNT] AS glnm (NOLOCK) ON glnm.[id] = ptp.[gl_no]
        WHERE prf.perf_no = @perf_no AND ptp.price_type = @price_type_no


    /*  If the assigned gl number is null or blank, assign the default gl number to the return variable  */
        
        IF ISNULL(@return_gl,'') = ''
            SELECT @return_gl = @default_gl

    /*  If the assigned gl number is STILL null or blank, return the string "Unknown Account"  */

        IF ISNULL(@return_gl,'') = ''
            SELECT @return_gl = 'Unknown Account'

    FINISHED:
    
        RETURN @return_gl

END
GO

GRANT EXECUTE ON [dbo].[LFS_GET_PERFORMANCE_GL_ACCOUNT] TO ImpUsers

--    SELECT [dbo].[LFS_GET_PERFORMANCE_GL_ACCOUNT] (49542, 0)



--SELECT MIN(ppt.price_type)
--FROM T_PERF_PRICE ppr
--     LEFT OUTER JOIN [dbo].[T_PERF_PRICE_TYPE] AS ppt ON ppt.[id] = ppr.[perf_price_type]
--WHERE ppr.perf_no = 49542 AND ppr.start_price > 0.0
--SELECT * FROM dbo.TR_PRICE_TYPE WHERE id = 3


--SELECT lay.*
--FROM [dbo].[T_PERF] AS prf (NOLOCK)
--     INNER JOIN [dbo].[T_PERF_PRICE] AS pri (NOLOCK) ON pri.[perf_no] = prf.[perf_no] --AND pri.[zone_no] = prf.[performance_zone]
--     INNER JOIN [dbo].[T_PERF_PRICE_TYPE] AS ptp (NOLOCK) ON ptp.[id] = pri.[perf_price_type]
--     INNER JOIN [dbo].[T_PERF_PRICE_LAYER] AS lay (NOLOCK) ON lay.[id] = ptp.[perf_price_layer]
--     LEFT OUTER JOIN [dbo].[TX_TEMPLATE_PRICE_TYPE] AS xtp (NOLOCK) ON xtp.[template] = lay.[template] AND xtp.[price_type] = ptp.[price_type]
--     INNER JOIN [dbo].[TR_PRICE_TYPE] AS typ (NOLOCK) ON typ.[id] = ptp.[price_type]
--     LEFT OUTER JOIN [dbo].[T_GL_ACCOUNT] AS gldf (NOLOCK) ON gldf.[id] = xtp.[gl_no]
--     INNER JOIN [dbo].[T_GL_ACCOUNT] AS glnm (NOLOCK) ON glnm.[id] = ptp.[gl_no]
--WHERE prf.perf_no = 49879 AND ptp.price_type = 3