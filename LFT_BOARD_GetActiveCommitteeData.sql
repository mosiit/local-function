USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

alter FUNCTION [dbo].[LFT_BOARD_GetActiveCommitteeData]
(
	@customer_no INT,
	@committee_status_filter VARCHAR(50) = 'Active' -- All or Active
)
RETURNS @CommitteeTbl TABLE 
(
	CommitteeName VARCHAR(100),
	Title VARCHAR(50),
	StartDate DATETIME,
	EndDate DATETIME,
	CommitteeStatus VARCHAR(30),
	MeetingAttended INT,
	MeetingDidNotAttend INT,
	MeetingsInvited INT
)
AS 
BEGIN

-- When development of this report began we were only looking at ACTIVE committee data, thus the name of the SP. 
-- The requirements changed but the name did not. 

-- [BEGIN] GET FY START AND END DATES 
	DECLARE @fyear INT,
	@FYStartDt DATETIME,
	@FYEndDt DATETIME 
	
    SELECT @fyear = fyear
    FROM dbo.TR_BATCH_Period
    WHERE GETDATE() BETWEEN start_dt AND end_dt

    SELECT @FYStartDt = MIN(start_dt), @FYEndDt = MAX(end_dt)
    FROM TR_BATCH_PERIOD 
    WHERE inactive = 'n' AND fyear = @fyear
-- [END] GET FY START AND END DATES 

------------------------- Get ACTIVE Committee Data -----------------------------------------------

INSERT INTO @CommitteeTbl
    (CommitteeName,
    Title,
    StartDate,
	EndDate,
	CommitteeStatus,
    MeetingAttended,
    MeetingDidNotAttend,
    MeetingsInvited)
SELECT comm.[associated_constituent_display_name] AS committee, comm.title, comm.start_dt, comm.end_dt, comm.relationship_type_description,
	meetingAttendance.Meeting_Attended, meetingAttendance.Meeting_Did_Not_Attend, meetingAttendance.total
FROM 
(
SELECT relationship_context, relationship_category_description, relationship_type_description, com.customer_no, 
		associated_constituent_display_name, note, start_dt, end_dt, primary_ind, title
	FROM [VS_RELATIONSHIP] com
	WHERE customer_no = @customer_no AND 
	 relationship_category_description = 'Committee' AND relationship_type_description = 'Active Committee'
) AS comm 
LEFT JOIN 
(
	-- Attendance of Committee Meetings in Current FY 
	SELECT associated_constituent_display_name, ISNULL([Meeting_Attended], 0) AS Meeting_Attended, ISNULL([Meeting_Did Not Attend], 0) AS Meeting_Did_Not_Attend, 
		ISNULL([Meeting_Attended], 0) + ISNULL([Meeting_Did Not Attend], 0) AS total
	FROM 
	(
		SELECT associated_constituent_display_name, relationship_type_description, COUNT(*) AS cnt
		FROM VS_RELATIONSHIP rel
		WHERE customer_no = @customer_no AND 
			 relationship_category_description = 'Committee meeting' AND start_dt BETWEEN @FYStartDt AND @FYEndDt
		GROUP BY associated_constituent_display_name, relationship_type_description
	) X
	PIVOT (MAX(cnt) FOR relationship_type_description IN ([Meeting_Attended], [Meeting_Did Not Attend]) ) AS piv
) AS meetingAttendance
ON comm.associated_constituent_display_name = meetingAttendance.associated_constituent_display_name

------------------------- Get FORMER Committee Data if requested and IF not currently Active -----------------------------------------------
IF @committee_status_filter = 'All'
BEGIN

	INSERT INTO @CommitteeTbl
	(
		CommitteeName,
		Title,
		StartDate,
		EndDate,
		CommitteeStatus
		)
	SELECT comm.[associated_constituent_display_name] AS committee, comm.title, comm.start_dt, comm.end_dt, comm.relationship_type_description
	FROM 
	(
	SELECT relationship_context, relationship_category_description, relationship_type_description, com.customer_no, 
			associated_constituent_display_name, note, start_dt, end_dt, primary_ind, title
		FROM [VS_RELATIONSHIP] com
		WHERE customer_no = @customer_no AND 
		 relationship_category_description = 'Committee' AND relationship_type_description = 'Former Committee'
		 -- If Board member is already ACTIVE, don't want to include an inactive record also.
		 AND [associated_constituent_display_name] NOT IN (SELECT CommitteeName FROM @CommitteeTbl) 
	) AS comm 

END 


RETURN;

END 
