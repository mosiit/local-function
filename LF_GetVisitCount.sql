USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LF_GetVisitCount]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
    DROP FUNCTION [dbo].[LF_GetVisitCount]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[LF_GetVisitCount] (@order_no INT = NULL) RETURNS INTEGER
AS BEGIN

        DECLARE @visit_count INT
        DECLARE @visit_count_raw_data TABLE ([perf_no] int, [zone_no] int, [title_name] VARCHAR(30), [prod_name] varchar(30), [sale_total] int)

        SELECT @order_no = ISNULL(@order_no,0)

        IF @order_no > 0 BEGIN

            INSERT INTO @visit_count_raw_data
            SELECT [perf_no], [zone_no], [title_name], [production_name], sum([sale_total]) 
            FROM [dbo].[LT_HISTORY_TICKET]
            WHERE [order_no] = @order_no
              AND ([title_name] in (SELECT [title_name] FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] WHERE [visit_count_title] = 'Y')
                   OR [production_name] IN (SELECT [production_name] FROM [dbo].[LV_PRODUCTION_ELEMENTS_PRODUCTION] WHERE [visit_count_production] = 'Y'))
            GROUP BY [perf_no], [zone_no], [title_name], [production_name]
       
            DELETE FROM @visit_count_raw_data WHERE prod_name like '%Buyout%'

            SELECT @visit_count = MAX([sale_total]) FROM @visit_count_raw_data

        END

        SELECT @visit_count = ISNULL(@visit_count,0)
        
        RETURN @visit_count

END
GO

GRANT EXECUTE ON [dbo].[LF_GetVisitCount] TO ImpUsers
GO


--SELECT [dbo].[LF_GetVisitCount]  (430387)


/*
--FOR TESTING--
DECLARE @visit_count_raw_data TABLE ([perf_no] int, [zone_no] int, [title_name] VARCHAR(30), [prod_name] varchar(30), [sale_total] int)
INSERT INTO @visit_count_raw_data
SELECT [perf_no], [zone_no], [title_name], [production_name], sum([sale_total]) 
FROM [dbo].[LT_HISTORY_TICKET]
WHERE [order_no] = 430387
AND ([title_name] in (SELECT [title_name] FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] WHERE [visit_count_title] = 'Y')
     OR [production_name] IN (SELECT [production_name] FROM [dbo].[LV_PRODUCTION_ELEMENTS_PRODUCTION] WHERE [visit_count_production] = 'Y'))
GROUP BY [perf_no], [zone_no], [title_name], [production_name]
DELETE FROM @visit_count_raw_data WHERE prod_name like '%Buyout%'
SELECT * FROM @visit_count_raw_data
*/

