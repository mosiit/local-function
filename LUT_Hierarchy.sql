USE [impresario]
GO

/****** Object:  UserDefinedTableType [dbo].[LUT_Hierarchy]    Script Date: 1/13/2020 11:40:46 AM ******/
DROP TYPE [dbo].[LUT_Hierarchy]
GO

/****** Object:  UserDefinedTableType [dbo].[LUT_Hierarchy]    Script Date: 1/13/2020 11:40:46 AM ******/
CREATE TYPE [dbo].[LUT_Hierarchy] AS TABLE(
	[element_id] [INT] NOT NULL,
	[sequenceNo] [INT] NULL,
	[parent_ID] [INT] NULL,
	[Object_ID] [INT] NULL,
	[NAME] [NVARCHAR](2000) NULL,
	[StringValue] [NVARCHAR](MAX) NOT NULL,
	[ValueType] [VARCHAR](10) NOT NULL,
	PRIMARY KEY CLUSTERED 
(
	[element_id] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)
GO


