CREATE FUNCTION LFT_SPLIT_DILEMETED_STRING (@InputString VARCHAR(8000), @Delimiter VARCHAR(50)) RETURNS @Items TABLE (Item_no INT, Item VARCHAR(8000))
AS BEGIN

    DECLARE @Item_no INT, @DelimIndex INT
    DECLARE @Item VARCHAR(8000), @ItemList VARCHAR(8000)

    IF @Delimiter = ' ' BEGIN
        SET @Delimiter = ','
        SET @InputString = REPLACE(@InputString, ' ', @Delimiter)
    END

    IF (@Delimiter IS NULL OR @Delimiter = '') SET @Delimiter = ','

    SET @item_no = 1

    SET @ItemList = @InputString
    SET @DelimIndex = CHARINDEX(@Delimiter, @ItemList, 0)
   
    WHILE (@DelimIndex != 0) BEGIN
        SET @Item = SUBSTRING(@ItemList, 0, @DelimIndex)
        INSERT INTO @Items VALUES (@item_no, @Item)

        SELECT @item_no = (@item_no + 1)

        -- Set @ItemList = @ItemList minus one less item
        SET @ItemList = SUBSTRING(@ItemList, @DelimIndex+1, LEN(@ItemList)-@DelimIndex)
        SET @DelimIndex = CHARINDEX(@Delimiter, @ItemList, 0)

    END 

    IF @Item IS NOT NULL BEGIN

        SET @Item = @ItemList
        INSERT INTO @Items VALUES (@item_no, @Item)
               
     END ELSE BEGIN
     
        INSERT INTO @Items VALUES (@item_no, @InputString)

    END

      RETURN

END
GO


GRANT SELECT ON dbo.LFT_SPLIT_DILEMETED_STRING TO ImpUsers
GO

