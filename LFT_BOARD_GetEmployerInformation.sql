USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[LFT_BOARD_GetEmployerInformation]
(
	@customer_no INT,
	@Handbook INT 
)
RETURNS @EmployerTbl TABLE 
(
	JobTitle VARCHAR(100),
	Employer VARCHAR(100),
	BAddress1 VARCHAR(100),
	BAddress2 VARCHAR(100),
	BAddress3 VARCHAR(100),
	BCity VARCHAR(100),
	BStateCd VARCHAR(2),
	BZipCode VARCHAR(10),
	BPhone VARCHAR(20),
	BFax VARCHAR(20),
	BEmail VARCHAR(100),
	BNotes VARCHAR(500)
)
AS 
BEGIN

INSERT INTO @EmployerTbl
        (JobTitle,
         Employer,
         BAddress1,
         BAddress2,
		 BAddress3,
         BCity,
         BStateCd,
         BZipCode,
         BPhone,
         BFax,
		 BEmail,
		 BNotes)
SELECT ISNULL(LTRIM(RTRIM(a.title)), ' ') AS JobTitle, 
	ISNULL(emp.lname, ' ') AS Employer,
	ISNULL(addr.Address1, ' ') AS Address1,
	ISNULL(addr.Address2, ' ') AS Address2,
	ISNULL(addr.Address3, ' ') AS Address3, 
	ISNULL(addr.City, ' ') AS city,
	ISNULL(addr.statecd, ' ') AS STATE, 
	ISNULL(addr.zipcode, ' ') AS postalCode,
	ISNULL(addr.phone, ' ' ) AS phone,
	ISNULL(addr.fax, ' ') AS fax, 
	ISNULL(eaddr.address, ' ') AS email,
	ISNULL(addr.notes , ' ') AS notes
FROM dbo.T_AFFILIATION a
INNER JOIN dbo.T_CUSTOMER emp
	ON emp.customer_no = a.group_customer_no
LEFT JOIN [dbo].[T_EADDRESS] eaddr
	ON a.individual_customer_no = eaddr.customer_no
	AND eaddr.eaddress_type = 5 -- Business 
	AND eaddr.inactive = 'N'
OUTER APPLY dbo.LFT_BOARD_GetAddress(a.individual_customer_no, 'Business', @Handbook) addr
WHERE a.affiliation_type_id = 10007 -- Employee_Main
	AND a.individual_customer_no = @customer_no 
	AND a.inactive = 'N'

RETURN;

END 
