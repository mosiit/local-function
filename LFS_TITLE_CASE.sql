USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LFS_TITLE_CASE]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
    DROP FUNCTION [dbo].[LFS_TITLE_CASE]
GO

    /*  Takes in a string and capitalizes each work in the string.

        At the end of the function a specific list of exception words are made lower case if they exist.
        This is probably not the full list of words that should not be capitalized, but they are the most common.
        If necessary, new words can be added to the list.

        Numbers, spaces, and special characters are left as they are and not changed.
        
    */
    CREATE FUNCTION [dbo].[LFS_TITLE_CASE] (@check_value VARCHAR(4000)) Returns VARCHAR(4000)
    AS BEGIN

        /* Declare variables  */   

            DECLARE @char_num INT = 1, @return_value VARCHAR(2000) = ''
    
            DECLARE @char_table TABLE ([id] INT IDENTITY(1,1) NOT NULL,             [char_val] CHAR(1) NOT NULL DEFAULT(''), 
                                       [char_type] CHAR(1)NOT NULL DEFAULT(''),     [char_ascii] INT NOT NULL DEFAULT(0),
                                       [prev_val] CHAR(1) NOT NULL DEFAULT(''),     [prev_type] CHAR(1) NOT NULL DEFAULT(''),
                                       [prev_ascii] INT NOT NULL DEFAULT(0))

        /*  If nothing passed to the function, jump to the end  */

            IF ISNULL(@check_value,'') = '' GOTO FINISHED
        
       /*  Add a single space to the beginning of the string  */

            SET @check_value = ' ' + LTRIM(RTRIM(@check_value))

        /*  Break out individual characters of the string  */
               
            WHILE @char_num <= LEN(@check_value) BEGIN

                INSERT INTO @char_table ([char_val], [char_type], [char_ascii], [prev_val], [prev_type], [prev_ascii])
                SELECT  SUBSTRING(@check_value,@char_num,1),
                        CASE WHEN ASCII(SUBSTRING(@check_value,@char_num,1)) BETWEEN 65 AND 90 THEN 'U'
                             WHEN ASCII(SUBSTRING(@check_value,@char_num,1)) BETWEEN 97 AND 122 THEN 'L'
                             WHEN ASCII(SUBSTRING(@check_value,@char_num,1)) = 32 THEN 'S'
                             ELSE 'N' END,
                        ASCII(SUBSTRING(@check_value,@char_num,1)),
                        CASE WHEN @char_num = 1 THEN ''
                             ELSE SUBSTRING(@check_value,(@char_num - 1),1) END,
                        CASE WHEN @char_num = 1 THEN ''
                             WHEN ASCII(SUBSTRING(@check_value,(@char_num - 1),1)) BETWEEN 65 AND 90 THEN 'U'
                             WHEN ASCII(SUBSTRING(@check_value,(@char_num - 1),1)) BETWEEN 97 AND 122 THEN 'L'
                             WHEN ASCII(SUBSTRING(@check_value,(@char_num - 1),1)) = 32 THEN 'S'
                             ELSE 'N' END,
                        CASE WHEN @char_num = 1 THEN 0
                             ELSE ASCII(SUBSTRING(@check_value,(@char_num - 1),1)) END
            
                SELECT @char_num += 1
    
             END

        /*  Set any letter that has a speace or hyphen (-) before it to upper case  */

            UPDATE @char_table SET [char_val] = UPPER(char_val) WHERE prev_ascii IN (32,45)  

        /*  Set any letter that does not have a space or hyphen (-) before it to lower case  */

            UPDATE @char_table SET [char_val] = LOWER(char_val) WHERE prev_ascii NOT IN (32,45)

        /*  Recombine the indivudual characters into one line
            The WHERE id > 1 removes the space at the beginning  */
        
             SELECT @return_value = (STUFF((SELECT '' + char_val
                                           FROM @char_table
                                           WHERE id > 1
                                           FOR XML PATH('')), 1, 0, ''))
    
        /*  Some special character (for some reason) come back as control codes that need to be converted  */

            SELECT @return_value = REPLACE(@return_value,'&#x20;',' ') --space
            SELECT @return_value = REPLACE(@return_value,'&lt;','<')
            SELECT @return_value = REPLACE(@return_value,'&gt;','>')
            SELECT @return_value = REPLACE(@return_value,'&amp;','&')
            
   
        /*  Exceptions - Add any specific exceptions to the capitalization rules to this section
                         These words will not be capitalized unless they are the first word  */

            --String should never end with one of these words, but a space is added to the end just in case it does
            --Without the space, any of these words as the last word in the string would not be changed to lower case
            --The space is removed in the RETURN statement
            SELECT @return_value = RTRIM(@return_value) + ' '

            SELECT @return_value = REPLACE(@return_value,' A ',' a ')
            SELECT @return_value = REPLACE(@return_value,' An ',' an ')
            SELECT @return_value = REPLACE(@return_value,' And ',' and ')
            SELECT @return_value = REPLACE(@return_value,' At ',' at ')
            SELECT @return_value = REPLACE(@return_value,' But ',' but ')
            SELECT @return_value = REPLACE(@return_value,' By ',' by ')
            SELECT @return_value = REPLACE(@return_value,' From ',' from ')
            SELECT @return_value = REPLACE(@return_value,' For ',' for ')
            SELECT @return_value = REPLACE(@return_value,' Nor ',' nor ')
            SELECT @return_value = REPLACE(@return_value,' Of ',' of ')
            SELECT @return_value = REPLACE(@return_value,' On ',' on ')
            SELECT @return_value = REPLACE(@return_value,' Or ',' or ')
            SELECT @return_value = REPLACE(@return_value,' The ',' the ')
            SELECT @return_value = REPLACE(@return_value,' To ',' to ')

        /*  Other Exceptions */

            SELECT @return_value = REPLACE(@return_value,'Po Box ','PO Box ')
        
        FINISHED:

            IF ISNULL(@return_value,'') = '' SELECT @return_value = LTRIM(ISNULL(@check_value,''))

            RETURN LTRIM(RTRIM(@return_value))

END
GO

GRANT EXECUTE ON [dbo].[LFS_TITLE_CASE] TO ImpUsers
GO

--SELECT [dbo].[LFS_TITLE_CASE]('; : '' " , . / ?  THIS IS A TEST to check THE TITLE < -- > cAPAtiZAtion FUNCTIOn!!!')

