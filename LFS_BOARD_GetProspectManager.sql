USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[LFS_BOARD_GetProspectManager](@customer_no INT)
RETURNS VARCHAR(100) AS

BEGIN 

	DECLARE @ProspectManager VARCHAR(100) = ''

	SELECT @ProspectManager = SUBSTRING(key_value, CHARINDEX(' ', key_value)+1, LEN(key_value)) 
	FROM dbo.TX_CUST_KEYWORD
	WHERE keyword_no = 555
		AND customer_no IN (SELECT expanded_customer_no FROM dbo.V_CUSTOMER_WITH_PRIMARY_GROUP WHERE customer_no = @customer_no)

	RETURN @ProspectManager

END 
GO

