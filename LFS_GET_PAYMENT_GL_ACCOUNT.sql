USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LFS_GET_PAYMENT_GL_ACCOUNT]') AND type IN (N'FN', N'IF', N'TF', N'FS', N'FT'))
    DROP FUNCTION [dbo].[LFS_GET_PAYMENT_GL_ACCOUNT]
GO

/*  pass it a customer numner and it will return the customer's full name using the first, middle, and last name values  */

CREATE FUNCTION [dbo].[LFS_GET_PAYMENT_GL_ACCOUNT](@pay_no INT,  @seq_no INT, @rtn_val VARCHAR(20)) RETURNS VARCHAR(30) 
AS BEGIN 

        DECLARE @gl_no VARCHAR(30) = '',
                @gl_descrip VARCHAR(30) = '',
                @return_value VARCHAR(30) = ''

        IF ISNULL(@pay_no,0) = 0 OR ISNULL(@seq_no,0) = 0 GOTO FINISHED
        
        IF @rtn_val NOT IN ('gl_descrip','gl_description') 
            SELECT @rtn_val = 'gl_no'


        SELECT @gl_no = ISNULL(gln.[gl_account_no],''),
               @gl_descrip = ISNULL(gln.[gl_description],'')
        FROM [dbo].[LV_PERFORMANCE_PAYMENT_INFO] AS pay
             LEFT OUTER JOIN [dbo].[T_PERF_PRICE_TYPE] AS pty ON pty.[id] = pay.pmap_no
             LEFT OUTER JOIN [dbo].[T_GL_ACCOUNT] AS gln ON gln.[id] = pty.[gl_no]
        WHERE pay.[payment_no] = @pay_no AND pay.[sequence_no] = @seq_no

        
        SELECT @return_value = CASE WHEN @rtn_val IN ('gl_descrip','gl_description') THEN @gl_descrip
                                    ELSE @gl_no END

    FINISHED:

        RETURN @return_value

END
GO


GRANT EXECUTE ON [dbo].[LFS_GET_PAYMENT_GL_ACCOUNT] TO ImpUsers
GO

       
--SELECT [dbo].[LFS_GET_PAYMENT_GL_ACCOUNT] (1394447, 2142754, 'gl_no'), [dbo].[LFS_GET_PAYMENT_GL_ACCOUNT] (1394447, 2142754, 'gl_descrip')
--UNION SELECT [dbo].[LFS_GET_PAYMENT_GL_ACCOUNT] (1394447, 2142755, 'gl_no'), [dbo].[LFS_GET_PAYMENT_GL_ACCOUNT] (1394447, 2142755, 'gl_descrip')
--UNION SELECT [dbo].[LFS_GET_PAYMENT_GL_ACCOUNT] (1394447, 2142756, 'gl_no'), [dbo].[LFS_GET_PAYMENT_GL_ACCOUNT] (1394447, 2142756, 'gl_descrip')


