USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[LFT_BOARD_GetPersonalMembership]
(
	@customer_no INT 
)
RETURNS @MembershipTbl TABLE 
(
	PersonalMembType VARCHAR(30),
	PersonalMembExpirationDate datetime
)
AS 
BEGIN

-- We want to prioritize Annual Fund Memberships over Household 
INSERT INTO @MembershipTbl (PersonalMembType, PersonalMembExpirationDate)
SELECT TOP 1 ml1.description, cm.expr_dt
FROM dbo.TX_CUST_MEMBERSHIP cm
	INNER JOIN dbo.T_MEMB_LEVEL ml1 
	ON cm.memb_org_no = ml1.memb_org_no AND cm.memb_level = ml1.memb_level
WHERE current_status = 2 AND cur_record = 'Y'
	AND customer_no IN (SELECT expanded_customer_no FROM dbo.V_CUSTOMER_WITH_PRIMARY_GROUP WHERE customer_no = @customer_no)
	AND ml1.category IN (2, 5, 10) -- Annual Fund, Household, Innovator
ORDER BY cm.category_trend -- if Annual Fund and Household both exist, show Annual Fund membership


RETURN;

END
