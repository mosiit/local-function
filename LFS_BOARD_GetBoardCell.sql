USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[LFS_BOARD_GetBoardCell](@customer_no INT, @Handbook INT)
RETURNS VARCHAR(100) AS

BEGIN 

-- Only want to pull cell information from the INDIVIDUAL record (not Household)
-- If @Handbook = 1 (being called from LRP_BOARD_ActiveBoardHandbook) and CPP = "Do Not Publish in Handbook", then return a blank
-- Otherwise, pull the Mobile Phone record (top 1) 

	DECLARE @cell VARCHAR(100) = '';

	WITH NotesCTE
	AS 
	(
		SELECT cpp.contact_point_id, purpose.description purpose, cat.description category, cpp.purpose_id
		FROM dbo.TX_CONTACT_POINT_PURPOSE cpp
		INNER JOIN dbo.TR_CONTACT_POINT_PURPOSE purpose
			ON purpose.id = cpp.purpose_id
		INNER JOIN dbo.TR_CONTACT_POINT_CATEGORY cat
			ON cat.id = cpp.contact_point_category
		WHERE purpose.id = 15 -- Do Not Publish in Handbook
			AND cat.id = -3 -- Phone
			AND purpose.inactive ='N'
	)
		-- if CPP says "Do Not Publish In Handbook", then return a blank string, else return the phone number 
		SELECT TOP 1 @cell = CASE WHEN @Handbook = 1 AND n.purpose_id  = 15 THEN '' ELSE dbo.AF_FORMAT_STRING(p.phone, '@@@-@@@-@@@@ @@@@') END 
		FROM T_PHONE p
		INNER JOIN dbo.TR_PHONE_TYPE ty
			ON p.type = ty.id
			AND p.type = 5 -- Mobile Phone 
		LEFT JOIN NotesCTE n
			ON p.phone_no = n.contact_point_id
			AND n.category = 'Phone'
		WHERE p.customer_no = @customer_no

RETURN @cell 

END

GO