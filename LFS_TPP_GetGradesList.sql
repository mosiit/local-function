USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[LFS_TPP_GetGradesList](@customer_no INT)
RETURNS VARCHAR(1000) AS

-- USED TO CREATE A LIST OF GRADES GIVEN A CUSTOMER_NO. 
-- THE LOW AND HIGH GRADES (TO DETERMINE THE RANGE) ARE STORED IN ATTRIBUTES. 

BEGIN
	DECLARE @GradeList VARCHAR(1000),
	@Low INT,
	@High INT

	SELECT @Low = id
	FROM TX_CUST_KEYWORD k
		INNER JOIN dbo.T_KWCODED_VALUES v
			ON k.key_value = v.key_value
			AND v.keyword_no = 523  -- looking at Lowest Grade List 
	WHERE customer_no = @customer_no
		AND k.keyword_no = 523 -- P1 Lowest Grade for customer_no

	SELECT @high = id
	FROM TX_CUST_KEYWORD k
		INNER JOIN dbo.T_KWCODED_VALUES v
			ON k.key_value = v.key_value
			AND v.keyword_no = 523 -- looking at Lowest Grade List 
	WHERE customer_no = @customer_no
		AND k.keyword_no = 524 -- P1 Highest Grade for customer_no

	SELECT @GradeList = COALESCE(@GradeList + ',', '') + key_value
	FROM dbo.T_KWCODED_VALUES
	WHERE id BETWEEN @Low AND @High 
	ORDER BY id 

	RETURN @GradeList
END

GO


