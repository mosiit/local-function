USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[LRP_BOARD_GetCommitteeMeetingsAttendedWithDates]
(
	@customer_no INT 
)
AS 
BEGIN

SELECT rel.associated_constituent_display_name, rel.title, rel.start_dt
	FROM VS_RELATIONSHIP rel
	WHERE rel.customer_no = @customer_no AND 
			rel.relationship_category_description = 'Committee meeting' AND rel.relationship_type_description = 'Meeting_Attended'
	ORDER BY rel.start_dt DESC
   
END 
