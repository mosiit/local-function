USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[LF_GetCapacity] (@perf_no INT = 0, @zone_no INT = 0, @capacity_type varchar(10) = 'total') RETURNS INTEGER
AS BEGIN

    DECLARE @return_capacity INT = 0
    DECLARE @tot INT = 0, @hld INT = 0, @cap INT = 0, @sld INT = 0, @avl INT = 0, @att INT = 0
    
    SELECT @perf_no = ISNULL(@perf_no,0), 
           @zone_no = ISNULL(@zone_no,0), 
           @capacity_type = ISNULL(@capacity_type,'total')
    
    IF @perf_no = 0 OR @zone_no = 0 GOTO FINISHED

    IF @capacity_type NOT IN ('Total','available','sold','capacity','held','hold','attend','attended','scan','scanned') GOTO FINISHED

    --TOTAL VALID TICKETS FROM PERF SEAT TABLE        
    SELECT @tot = COUNT([seat_no]) 
                  FROM [dbo].[TX_PERF_SEAT] 
                  WHERE [perf_no] = @perf_no 
                    AND [zone_no] = @zone_no 
                    AND [seat_status] NOT IN (5,6,9)

    --TICKETS CURRENTLY BEING HELD FOR WHATEVER REASON IN THE PERF SEAT TABLE
    IF @capacity_type IN ('held','hold','capacity','available')
        SELECT @hld = COUNT([seat_no]) 
                      FROM [dbo].[TX_PERF_SEAT] 
                      WHERE [perf_no] = @perf_no 
                      AND [zone_no] = @zone_no 
                      AND [seat_status] IN (1,4) 

    --FOR TICKETS SOLD, IF IT'S A PAST DATE, USE HISTORY.  FOR TODAY OR FUTURE DATE, USE PERF SEAT TABLE
    IF @capacity_type IN ('sold', 'available') BEGIN

        SELECT @sld = COUNT([seat_no]) 
                      FROM [dbo].[TX_PERF_SEAT] 
                      WHERE [perf_no] = @perf_no 
                        AND [zone_no] = @zone_no 
                        AND [seat_status] IN (3,7,8,13)
    
    END

    IF @capacity_type in ('attend','attended','scan','scanned') BEGIN

        SELECT @att = ISNULL(SUM((ISNULL(att.[admission_adult],0) + ISNULL(att.[admission_child],0) + ISNULL(att.[admission_other],0))),0)
        FROM [dbo].[TX_PERF_SEAT] AS prf
             INNER JOIN [dbo].[T_SUB_LINEITEM] AS sli ON sli.[sli_no] = ISNULL(prf.[sli_no],0)
             INNER JOIN [dbo].[T_ATTENDANCE] AS att ON att.[ticket_no] = sli.[ticket_no]
        WHERE prf.[perf_no] = @perf_no 
          AND prf.[zone_no] = @zone_no 

    END              
    
    --TO THE MATH TO GET SELLABLE CAPACITY AND AVAILABLE CAPACITY
    SELECT @cap = (@tot - @hld)
    SELECT @avl = ((@tot - @hld) - @sld)

    --SELECT THE CORRECT VALUE TO RETURN
    SELECT @return_capacity = CASE WHEN @capacity_type = 'available' THEN @avl
                                   WHEN @capacity_type = 'sold' THEN @sld
                                   WHEN @capacity_type = 'capacity' THEN @cap
                                   WHEN @capacity_type IN ('held','hold') THEN @hld
                                   WHEN @capacity_type IN ('attend','attended','scan','scanned') THEN @att
                                   ELSE @tot END

    FINISHED:

        RETURN @return_capacity                               

END
GO

GRANT EXECUTE ON [dbo].[LF_GetCapacity] TO ImpUsers, tessitura_app
GO


/*
--FOR TESTING
--DECLARE @perf_no INT = 50607, @zone_no INT = 610
--DECLARE @perf_no INT = 50702, @zone_no INT = 610
DECLARE @perf_no INT = 82984, @zone_no INT = 712
SELECT performance_no, performance_zone, title_name, production_name, performance_date, performance_time
FROM LV_PRODUCTION_ELEMENTS_PERFORMANCE WHERE performance_no = @perf_no and performance_zone = @zone_no
SELECT [dbo].[LF_GetCapacity] (@perf_no, @zone_no, 'total') AS 'Total', 
       [dbo].[LF_GetCapacity] (@perf_no, @zone_no, 'held') AS 'Held',
       [dbo].[LF_GetCapacity] (@perf_no, @zone_no, 'capacity') AS 'Capacity',
       [dbo].[LF_GetCapacity] (@perf_no, @zone_no, 'sold') AS 'Sold',
       [dbo].[LF_GetCapacity] (@perf_no,@zone_no , 'available') AS 'Available',
       [dbo].[LF_GetCapacity] (@perf_no,@zone_no , 'scanned') AS 'Attended'

--SELECT DISTINCT performance_no, performance_zone, performance_time, production_name FROM dbo.LV_PRODUCTION_ELEMENTS_PERFORMANCE WHERE performance_date = '2020/07/22' AND title_no = 27
--SELECT DISTINCT performance_no, performance_zone FROM dbo.LV_PRODUCTION_ELEMENTS_PERFORMANCE WHERE performance_date = '2019/10/16' AND title_no = 37
*/


--SELECT * FROM [dbo].[TX_PERF_SEAT] WHERE [perf_no] = 82984 AND [zone_no] = 712 AND [seat_status] NOT IN (5,6,9)