USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LFS_GET_ORDER_VISIT_COUNT]') AND type IN (N'FN', N'IF', N'TF', N'FS', N'FT'))
    DROP FUNCTION [dbo].[LFS_GET_ORDER_VISIT_COUNT]
GO

/*  pass it a, order numner and it will the visit count for that particular order.  Based on sales, not scanned attendance.   */

CREATE FUNCTION [dbo].[LFS_GET_ORDER_VISIT_COUNT] (@order_no int) Returns int
AS BEGIN

    /*  Function Parameters  */

        DECLARE @order_mos INT = 0;
        DECLARE @return_visit_count INT = 0;
        DECLARE @order_info TABLE ([order_no] INT,[performance_dt] DATETIME, [Exhibit Halls] INT, [Mugar Omni Theater] INT, [Hayden Planetarium] INT, [Special Exhibitions] INT);

    /* Check for the Buyouts mode of sale */

        SELECT @order_mos =ISNULL([mos],0) FROM [dbo].[T_ORDER] WHERE order_no = @order_no;
        
        IF @order_mos = (SELECT [id] FROM [dbo].[TR_MOS] WHERE [description] = 'Buyouts')
            GOTO FINISHED;

    /*  Pull performance information for the order into a pivot table  */

        WITH CTE_PERF_NO ([perf_no], [perf_dt])
        AS (SELECT DISTINCT sli.[perf_no], prf.[performance_dt]
            FROM [dbo].[T_SUB_LINEITEM] AS sli
                 INNER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf ON prf.[performance_no] = sli.[perf_no] 
                                                                             AND prf.[performance_zone] = sli.[zone_no]
            WHERE sli.order_no = @order_no
              AND prf.[title_name] IN ('Exhibit Halls', 'Mugar Omni Theater', 'Hayden Planetarium', 'Special Exhibitions')
            )
                INSERT INTO @order_info ([order_no], [performance_dt], [Exhibit Halls], [Mugar Omni Theater], [Hayden Planetarium], [Special Exhibitions])
                SELECT [order_no], 
                       [performance_dt], 
                       [Exhibit Halls], 
                       [Mugar Omni Theater], 
                       [Hayden Planetarium], 
                       [Special Exhibitions]
                FROM (SELECT sli.[sli_no], 
                             sli.[order_no], 
                             prf.[performance_dt], 
                             prf.[title_name]
                      FROM [dbo].[T_SUB_LINEITEM] AS sli
                           LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] AS prf ON prf.[performance_no] = sli.[perf_no] AND prf.[performance_zone] = sli.[zone_no]
                           INNER JOIN CTE_PERF_NO AS ctp ON ctp.[perf_no] = prf.[performance_no] AND ctp.[perf_dt] = prf.[performance_dt]
                      WHERE sli.[order_no] = @order_no 
                        AND sli.[sli_status] IN (2, 3, 12)
                        AND prf.[production_name] NOT LIKE '%Buyout%'
                        AND prf.[production_name] <> 'Exhibit Halls Special') AS D
                PIVOT (COUNT(sli_no) FOR [title_name] IN ([Exhibit Halls], [Mugar Omni Theater], [Hayden Planetarium],[Special Exhibitions])) AS pvt;

    /*  Get the maximum count from the order info (unpivot the summed data to get it)  */

        SELECT @return_visit_count = MAX([visit_count])
        FROM @order_info
        UNPIVOT (visit_count FOR venue IN ([Exhibit Halls], [Mugar Omni Theater], [Hayden Planetarium], [Special Exhibitions])) AS unpvt
        GROUP BY order_no;

    FINISHED:

        /*  Return final value (return zero if null)  */

            RETURN ISNULL(@return_visit_count,0);

END
GO

GRANT EXECUTE ON [dbo].[LFS_GET_ORDER_VISIT_COUNT] TO [ImpUsers] AS [dbo]
GO

SELECT [dbo].[LFS_GET_ORDER_VISIT_COUNT] (1558249)




