USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/*  Takes a list of id numbers and returns a list of the actual names of the items
    Works for titles, productions, production seasons, performances (returns perf code), modes of sales and sales channels  */

ALTER FUNCTION [dbo].[LFS_CreateNameList] (@list_type VARCHAR(50), @id_list VARCHAR(2000)) RETURNS VARCHAR(8000)
AS BEGIN

	DECLARE @NameList VARCHAR(8000), @list_name varchar(100)
    DECLARE @id_table TABLE ([id_str] VARCHAR(25), [id_no] INT)
    
    INSERT INTO @id_table (id_str) SELECT LTRIM(RTRIM(Item)) FROM [dbo].[LFT_SPLIT_DILEMETED_STRING] (@id_list,',')
    DELETE FROM @id_table WHERE dbo.FS_isReallyNumeric([id_str]) = 0
    UPDATE @id_table SET [id_no] = CAST([id_str] AS INT)

         IF @list_type = 'title' DECLARE name_cursor INSENSITIVE CURSOR FOR SELECT [title_name] FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_TITLE] (NOLOCK) WHERE [title_no] IN (SELECT [id_no] FROM @id_table)
    ELSE IF @list_type = 'production' DECLARE name_cursor INSENSITIVE CURSOR FOR SELECT [production_name] FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_PRODUCTION] (NOLOCK) WHERE [production_no] IN (SELECT [id_no] FROM @id_table)
    ELSE IF @list_type = 'season' DECLARE name_cursor INSENSITIVE CURSOR FOR SELECT [production_season_name] FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_SEASON] (NOLOCK) WHERE [production_season_no] IN (SELECT [id_no] FROM @id_table)
    ELSE IF @list_type = 'performance' DECLARE name_cursor INSENSITIVE CURSOR FOR SELECT DISTINCT [performance_code] FROM [dbo].[LV_SS_PRODUCTION_ELEMENTS_PERFORMANCE] (NOLOCK) WHERE [performance_no] IN (SELECT [id_no] FROM @id_table)
    ELSE IF @list_type = 'mos' DECLARE name_cursor INSENSITIVE CURSOR FOR SELECT DISTINCT [description] FROM [dbo].[TR_MOS] (NOLOCK) WHERE [id] IN (SELECT [id_no] FROM @id_table)
    ELSE IF @list_type = 'channel' DECLARE name_cursor INSENSITIVE CURSOR FOR SELECT DISTINCT [description] FROM [dbo].[TR_SALES_CHANNEL] (NOLOCK) WHERE [id] IN (SELECT [id_no] FROM @id_table)

    SELECT @NameList = ''

    OPEN name_cursor
    BEGIN_NAME_LOOP:

        FETCH NEXT FROM name_cursor INTO @list_name
        IF @@FETCH_STATUS = -1 GOTO END_NAME_LOOP

        IF @NameList <> '' SELECT @NameList = @NameList + ', '
        SELECT @NameList = @NameList + @list_name
        
        GOTO BEGIN_NAME_LOOP

    END_NAME_LOOP:
    CLOSE name_cursor
    DEALLOCATE name_cursor

	RETURN @NameList
    
END

GO


--DECLARE @rtn_value VARCHAR(8000)
----SELECT [dbo].[LFS_CreateNameList] ('title', '161,173')
--SELECT @rtn_value = [dbo].[LFS_CreateNameList] ('production', '1201,15268,18916,1203,1392,11943,1207,1208,18766,1210,15276,1211,1222,1223,1292,1224,1403,1397,11942')
--PRINT @rtn_value




