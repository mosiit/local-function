USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LF_GetFiscalYear]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
    DROP FUNCTION [dbo].[LF_GetFiscalYear]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[LF_GetFiscalYear] (@fDate DATETIME = NULL) RETURNS INTEGER
AS BEGIN

    DECLARE @fiscal_year INT = 0

    IF @fDate IS NOT NULL BEGIN
    
        SELECT @fiscal_year = MAX(fyear) FROM [dbo].[TR_Batch_Period] WHERE @fDate BETWEEN [start_dt] AND [end_dt]

    END

    RETURN @fiscal_year

END
 


GO

GRANT EXECUTE ON [dbo].[LF_GetFiscalYear] TO [ImpUsers] AS [dbo]
GO


