USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LF_GetFiscalYearEndDt]') AND type IN (N'FN', N'IF', N'TF', N'FS', N'FT'))
    DROP FUNCTION [dbo].[LF_GetFiscalYearEndDt]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[LF_GetFiscalYearEndDt] (@fiscal_year INT = NULL) RETURNS datetime
AS BEGIN

    DECLARE @return_dt DATETIME = null

    IF @fiscal_year IS NULL SELECT @fiscal_year = MAX([fyear]) FROM [dbo].[TR_Batch_Period] WHERE GETDATE() BETWEEN [start_dt] AND [end_dt]
    SELECT @fiscal_year = ISNULL(@fiscal_year,0)
    
    IF @fiscal_year > 0 SELECT @return_dt = max([end_dt]) FROM [dbo].[TR_Batch_Period] WHERE [fyear] = @fiscal_year

    FINISHED:   

        RETURN @return_dt

END
GO 

GRANT EXECUTE ON [dbo].[LF_GetFiscalYearEndDt] TO [ImpUsers] AS [dbo]
GO

