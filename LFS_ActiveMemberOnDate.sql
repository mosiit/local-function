USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LFS_ActiveMemberOnDate]') AND type IN (N'FN', N'IF', N'TF', N'FS', N'FT'))
    DROP FUNCTION [dbo].[LFS_ActiveMemberOnDate]
GO

/*  LFS_ActiveMemberOnDate
    Returns a Y or N indicating whether a specific customer had or will have a valid membership on a particular date
    There are four values passed to the function
       @customer_no = the customer to check
          @memb_org = a specific membership organization If 0, check all membership organizations
          @check_dt = Were they or will they be a valid member on this date?
      @start_of_day = Y/N - Make sure they were a valid member at the start of the day?
                            i.e. If a new membership is purchased on April 15 at 2:00 PM, they
                                 became a valid member as of 2:00 on Aptil 15 but they were not a 
                                 valid member at the start of the day.  If that matters, set this to 'Y'
    NOTE: Only memberships with a status of 1 (inactive), 2 (active) or 3 (pending) are looked at.
          Memberships with any other status like suspended, deactivated or merged will be ignored.
          
    Example:  SELECT [dbo].[LFS_ActiveMemberOnDate] (67226, 4, '7-1-2018', 'Y')  */

CREATE FUNCTION [dbo].[LFS_ActiveMemberOnDate] (@customer_no INT, @memb_org INT, @check_dt DATETIME, @start_of_day CHAR(1)) RETURNS CHAR(1)
AS BEGIN 

    /*  Function Variables  */

        DECLARE @active_member CHAR(1) = 'N'

        DECLARE @mem_table TABLE ([mem_no] INT, [mem_status] INT, [mem_org] INT, [mem_create] DATETIME, 
                                  [mem_init] DATETIME, [mem_expire] DATETIME)

    /*  Check Parameters  */

        --Set @check_dt to today if null and set time to 00:00:00
        SELECT @check_dt = ISNULL(@check_dt,GETDATE())

        SELECT @check_dt = CONVERT(DATE,@check_dt)

        --Set Default Values
        SELECT @memb_org = ISNULL(@memb_org,0)

        SELECT @start_of_day = ISNULL(@start_of_day,'Y')


    /*  If Custome No = 0, Jump to the End  */

        IF @customer_no = 0 GOTO FINISHED
        
    /*  Get all membership records for this customer - limit to specific organization if one was passed in
        Ignore records that aren't pending, active, or inactive - Remove times from all date/time values  */
        
        INSERT INTO @mem_table ([mem_no], [mem_status], [mem_org], [mem_create], [mem_init], [mem_expire])
            SELECT [cust_memb_no], 
                   [current_status], 
                   [memb_org_no], 
                   CONVERT(DATE,[create_dt]), 
                   CONVERT(DATE,[init_dt]), 
                   CONVERT(DATE,[expr_dt])
            FROM [dbo].[TX_CUST_MEMBERSHIP] 
            WHERE customer_no = @customer_no
              AND (@memb_org = 0 OR memb_org_no = @memb_org)
              AND current_status IN (1, 2, 3, 7)   --1=Inactive/2=Active/3=Pending/7=Lapsed

    /*  If no memberships found for this customer, jump to the end  */

        IF NOT EXISTS (SELECT * FROM @mem_table) GOTO FINISHED
 
    /* To determine if member that day, date passed must be between init date and expire date on an inactive, active, or pending membership  */

        IF EXISTS (SELECT * 
                   FROM @mem_table 
                   WHERE @check_dt BETWEEN [mem_init] AND [mem_expire]) BEGIN

            SELECT @active_member = 'Y'

            --If needs to be member at start of day, they also need to have a membership where (date passed - 1) 
            --is between init date and expire date.  If not, change back to No.
            IF @start_of_day = 'Y' AND NOT EXISTS (SELECT * 
                                                   FROM @mem_table 
                                                   WHERE DATEADD(DAY,-1,@check_dt) BETWEEN [mem_init] AND [mem_expire])
                SELECT @active_member = 'N'

        END

    /*  Deal with a null value - Default to No */

        SELECT @active_member = ISNULL(@active_member,'N')

    FINISHED:

        /*  Return final value  */

            RETURN @active_member

END
GO

GRANT EXECUTE ON [dbo].[LFS_ActiveMemberOnDate] TO ImpUsers
GO


