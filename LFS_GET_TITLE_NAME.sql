USE impresario
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LFS_GET_TITLE_NAME]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
    DROP FUNCTION [dbo].[LFS_GET_TITLE_NAME]
GO

/*  Pass it an inventory id number from any level (performance, production season, production, or title) and
    this function will return the name of the title (top most level of the tree).   */

CREATE FUNCTION [dbo].[LFS_GET_TITLE_NAME] (@inv_no int) Returns varchar(30)
AS BEGIN

    DECLARE @title_name varchar(30)
    DECLARE @title_no int, @prod_no int, @prod_season_no int, @perf_no int
    DECLARE @initial_type char(1)

    SELECT @title_name = ''

    SELECT @title_no = -1, @prod_no = -1, @prod_season_no = -1, @perf_no = -1

    SELECT @initial_type = [type] FROM [dbo].[T_INVENTORY] WHERE [inv_no] = @inv_no
    SELECT @initial_type = IsNull(@initial_type, 'X')

         IF @initial_type = 'R' SELECT @perf_no = @inv_no
    ELSE IF @initial_type = 'S' SELECT @prod_season_no = @inv_no
    ELSE IF @initial_type = 'P' SELECT @prod_no = @inv_no
    ELSE IF @initial_type = 'T' SELECT @title_no = @inv_no

    IF @perf_no > 0 SELECT @prod_season_no = [prod_season_no] FROM [dbo].[T_PERF] WHERE [perf_no] = @perf_no
    SELECT @prod_season_no = IsNull(@prod_season_no, -1)

    IF @prod_season_no > 0 SELECT @prod_no = [prod_no] FROM [dbo].[T_PROD_SEASON] WHERE [prod_season_no] = @prod_season_no
    SELECT @prod_no = IsNull(@prod_no, -1)

    IF @prod_no > 0 SELECT @title_no = [title_no] FROM [dbo].[T_PRODUCTION] WHERE [prod_no] = @prod_no
    SELECT @title_no = IsNull(@title_no, -1)

    IF @title_no > 0 SELECT @title_name = [description] FROM [dbo].[T_INVENTORY] WHERE [inv_no] = @title_no
    SELECT @title_name = IsNull(@title_name, '')

    IF @title_name = '' SELECT @title_name = 'Unknown'

    RETURN @title_name

END
GO

GRANT EXECUTE ON [dbo].[LFS_GET_TITLE_NAME] TO impusers
GO


--SELECT [dbo].[LFS_GET_TITLE_NAME] (8110)