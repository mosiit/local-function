USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[LFS_BOARD_GetBoardPhoneByType](@customer_no INT, @Handbook INT, @PhoneType VARCHAR(30))
RETURNS VARCHAR(100) AS

BEGIN 

-- If @Handbook = 1 (being called from LRP_BOARD_ActiveBoardHandbook) and CPP = "Do Not Publish in Handbook", then return a blank
-- Otherwise, pull the Phone record (top 1) 

-- Mobile Phone and Business Main/Direct have slightly different logic. There are separate SP for those and those SP can be called on their own.
-- However, in case someone calls this SP for Mobile Phone or Business Main/Direct, I want to make sure the correct logic is called. 

	DECLARE @phone VARCHAR(100) = '';

	IF @PhoneType = 'Mobile Phone'
		SELECT @phone = [dbo].[LFS_BOARD_GetBoardCell](@customer_no, @Handbook)
	ELSE IF @PhoneType = 'Business'
		SELECT @phone = [dbo].[LFS_BOARD_GetBoardBusinessPhone](@customer_no, @Handbook)
	ELSE -- all other Phone Types 
		WITH NotesCTE
		AS 
		(
			SELECT cpp.contact_point_id, purpose.description purpose, cat.description category, cpp.purpose_id
			FROM dbo.TX_CONTACT_POINT_PURPOSE cpp
			INNER JOIN dbo.TR_CONTACT_POINT_PURPOSE purpose
				ON purpose.id = cpp.purpose_id
			INNER JOIN dbo.TR_CONTACT_POINT_CATEGORY cat
				ON cat.id = cpp.contact_point_category
			WHERE purpose.id = 15 -- Do Not Publish in Handbook
				AND cat.id = -3 -- Phone
				AND purpose.inactive ='N'
		)
			-- if CPP says "Do Not Publish In Handbook", then return a blank string, else return the phone number 
			SELECT TOP 1 @phone = CASE WHEN @Handbook = 1 AND n.purpose_id  = 15 THEN '' ELSE dbo.AF_FORMAT_STRING(p.phone, '@@@-@@@-@@@@ @@@@') END 
			FROM T_PHONE p
			INNER JOIN dbo.TR_PHONE_TYPE ty
				ON p.type = ty.id
			INNER JOIN dbo.V_CUSTOMER_WITH_PRIMARY_AFFILIATES pri
				ON p.customer_no = pri.expanded_customer_no
			LEFT JOIN NotesCTE n
				ON p.phone_no = n.contact_point_id
				AND n.category = 'Phone'
			WHERE pri.customer_no = @customer_no
				AND ty.description = @PhoneType

	RETURN @phone 

END

GO