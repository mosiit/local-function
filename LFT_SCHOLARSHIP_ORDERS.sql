USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[LFT_SCHOLARSHIP_ORDERS] ()
RETURNS @scholarship_orders TABLE ([order_no] INT NOT NULL DEFAULT (0), 
                                   [mode_of_sale_category] VARCHAR(50) NOT NULL DEFAULT (''),
                                   [mode_of_sale] VARCHAR(50) NOT NULL DEFAULT (''),
                                   [perf_date] DATE NULL,
                                   [fiscal_year] VARCHAR(4) NOT NULL DEFAULT (''),
                                   [payment_method_no] INT NOT NULL DEFAULT (0),
                                   [payment_method] VARCHAR(50) NOT NULL DEFAULT (''),
                                   [scholarship_type] VARCHAR(50) NOT NULL DEFAULT (''),
                                   [scholarship_no] INT NOT NULL DEFAULT (0),
                                   [scholarship_name] VARCHAR(50) NOT NULL DEFAULT (''),
                                   [payment_user] VARCHAR(50) NOT NULL DEFAULT (''),
                                   [payment_department] VARCHAR(50) NOT NULL DEFAULT (''),
                                   [customer_no] INT NOT NULL DEFAULT (0),
                                   [customer_city] VARCHAR(50) NOT NULL DEFAULT (''),
                                   [customer_state] VARCHAR(50) NOT NULL DEFAULT (''),
                                   [customer_zip_code] VARCHAR(50) NOT NULL DEFAULT (''),
                                   [payment_amount] DECIMAL(18,2) NOT NULL DEFAULT (0.0))
AS BEGIN

        --Initial data selection
        WITH [CTE_MODE_OF_SALE] AS (SELECT mos.[id] AS [mode_of_sale_no],
                                           CASE WHEN cat.[description] like '%School%' OR mos.[description] like '%school%' THEN 'Schools' 
                                                ELSE cat.[description] END  AS [mode_of_sale_category],
                                           mos.[description] AS [mode_of_sale],
                                           mos.[general_public_ind]
                                    FROM [dbo].[TR_MOS] AS mos
                                         LEFT OUTER JOIN [dbo].[TR_MOS_CATEGORY] AS cat ON cat.[id] = mos.[category])     
        INSERT INTO @scholarship_orders ([order_no], [mode_of_sale_category], [mode_of_sale], [perf_date], [fiscal_year], [payment_method_no],
                                           [payment_method], [scholarship_type], [scholarship_no], [scholarship_name], [payment_user], [payment_department],
                                           [customer_no], [customer_city], [customer_state], [customer_zip_code], [payment_amount])
        
        SELECT sch.[order_no], 
               mos.[mode_of_sale_category],
               mos.[mode_of_sale],
               CAST(sch.[performance_dt] AS DATE),
               '',
               sch.[payment_method_no],
               sch.[payment_method],
               sch.[scholarship_type],
               sch.[scholarship_no],
               CASE WHEN sch.[scholarship_no] = 0 THEN 'No Scholarship Info'
                    ELSE sch.[scholarship_name] END,
               sch.[payment_created_by],
               usr.[location],
               sch.[customer_no],
               sch.[address_city],
               sch.[address_state],
               LEFT(sch.[address_zip_code],5),
               SUM(sch.[payment_amount])
        FROM [dbo].[LV_SCHOLARSHIP_PAYMENTS] AS sch
             INNER JOIN [dbo].[T_ORDER] AS ord ON ord.[order_no] = sch.[order_no]
             INNER JOIN [CTE_MODE_OF_SALE] AS mos ON mos.[mode_of_sale_no] = ord.[MOS]
             INNER JOIN [dbo].[LV_MuseumUserInfo] AS usr ON usr.[userid] = sch.[payment_created_by]
        GROUP BY sch.[order_no], mos.[mode_of_sale_category], mos.[mode_of_sale], CAST(sch.[performance_dt] AS DATE), sch.[payment_method_no], 
                 sch.[payment_method], sch.[scholarship_type], sch.[scholarship_no],sch.[scholarship_name], sch.[payment_created_by],
                 usr.[location], sch.[customer_no], sch.[address_city],sch.[address_state], LEFT(sch.[address_zip_code],5)
        HAVING SUM(sch.[payment_amount]) <> 0.0;

        --Delete On-Accounts and Interdepartmental Transfers from this data
        DELETE FROM @scholarship_orders 
        WHERE payment_method IN ('On Acct - Scholarship', 'Interdepartmental Transfer');

        --Get Fiscal Year based on performance date
        UPDATE @scholarship_orders
        SET [fiscal_year] = (SELECT MAX(bpr.fyear) FROM [dbo].[TR_BATCH_PERIOD] AS bpr WHERE [perf_date] BETWEEN [start_dt] AND [end_dt]);
        
        --Some perf dates coming over as 10/17/1971 with a fiscal year of 1972
        --If other records for same order exist in the data set, change to the max fiscal year and perf date from those other records
        UPDATE upd
        SET upd.[fiscal_year] = (SELECT MAX(sch.[fiscal_year]) 
                                 FROM @scholarship_orders AS sch 
                                 WHERE sch.[order_no] = upd.[order_no]),
            upd.[perf_date] = (SELECT MAX(sch.[perf_date]) 
                               FROM @scholarship_orders AS sch 
                               WHERE sch.[order_no] = upd.[order_no])
        FROM @scholarship_orders AS upd
        WHERE upd.[fiscal_year] < '2016';

        --10/17/1971 perf dates still exist
        --Use the products on the order to determine the correct perf date and fiscal year
        UPDATE upd
        SET upd.[perf_date] = (SELECT MAX(CAST(prf.[perf_dt] AS DATE))
                           FROM @scholarship_orders AS sch
                                LEFT OUTER JOIN [dbo].[T_SUB_LINEITEM] AS sli ON sli.[order_no] = sch.[order_no]
                                LEFT OUTER JOIN [dbo].[T_PERF] AS prf ON prf.[perf_no] = sli.[perf_no]
                                WHERE sch.[order_no] = upd.[order_no]),
            upd.[fiscal_year] = (SELECT ISNULL(MAX(per.fyear),0)
                             FROM @scholarship_orders AS sch
                                  LEFT OUTER JOIN [dbo].[T_SUB_LINEITEM] AS sli ON sli.[order_no] = sch.[order_no]
                                  LEFT OUTER JOIN [dbo].[T_PERF] AS prf ON prf.[perf_no] = sli.[perf_no]
                                  LEFT OUTER JOIN [dbo].[TR_BATCH_PERIOD] AS per ON prf.[perf_dt] BETWEEN per.[start_dt] AND per.[end_dt]
                             WHERE sch.[order_no] = upd.[order_no])
        FROM @scholarship_orders AS upd
        WHERE upd.[fiscal_year] < '2016' OR upd.[perf_date] < '5-18-2016';
             
        --Some order came over during the initial data conversion with a mode of sale of "Live Data Conversion"
        --Change accordingly based on what their payment method is
        UPDATE @scholarship_orders
        SET [mode_of_sale_category] = 'Schools',
            [mode_of_sale] = 'Schools'
        WHERE [mode_of_sale_category] = 'Administrative'
          AND ([payment_method] like '%Eye Opener%' OR [payment_method] LIKE '%SCH:%');
                
        UPDATE @scholarship_orders
        SET [mode_of_sale_category] = 'Ticketing',
            [mode_of_sale] = 'Groups'
        WHERE [mode_of_sale_category] = 'Administrative'
          AND [payment_method] like '%CommRel%';

        --When all is said and done, delete any orders from this table where the scholarship payments net out to $0.00
        DELETE FROM @scholarship_orders 
        WHERE [order_no] IN (SELECT [order_no] FROM @scholarship_orders 
                             GROUP BY [order_no] 
                             HAVING SUM([payment_amount]) = 0.0);

        --A record that still has no valid fiscal year means there are no products on the order. 
        --Delete any of these records that still exist
        DELETE FROM @scholarship_orders WHERE ISNULL([fiscal_year],'0') = '0';

        --Update the Bad Scholarship types
        
        ----Set to Invalid if a scholarship number is entered but it does not tie back to a valid scholarship
        UPDATE @scholarship_orders SET [scholarship_type] = 'Invalid' 
        WHERE scholarship_name LIKE 'Invalid Scholarship ID%'

        ----Set to Unknown if no scholarship number was entered at all
        UPDATE @scholarship_orders SET [scholarship_type] = 'Unknown' 
        WHERE scholarship_no = 0

        --Set All Invalid Scholarships to have the same name
        UPDATE @scholarship_orders SET [scholarship_name] = 'Invalid Scholarhip ID' 
        WHERE scholarship_name LIKE 'Invalid Scholarship ID%'

        RETURN;
END;
GO

GRANT SELECT ON [dbo].[LFT_SCHOLARSHIP_ORDERS] TO [impusers], [tessitura_app]
GO

SELECT * FROM [dbo].[LFT_SCHOLARSHIP_ORDERS] () 

