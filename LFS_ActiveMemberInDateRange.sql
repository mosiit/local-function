USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LFS_ActiveMemberInDateRange]') AND type IN (N'FN', N'IF', N'TF', N'FS', N'FT'))
    DROP FUNCTION [dbo].[LFS_ActiveMemberInDateRange]
GO

/*  LFS_ActiveMemberInDateRange
    Returns a Y or N indicating whether a specific customer had or will have a valid membership at any point during a particular date range
    There are four values passed to the function
       @customer_no = the customer to check
       @memb_org = a specific membership organization If 0, check all membership organizations
       @start_dt = Start of the date range to check
       @end_dt = End of the date range to check
    NOTE: Only memberships with a status of 1 (inactive), 2 (active) or 3 (pending) are looked at.
          Memberships with any other status like suspended, deactivated or merged will be ignored as if they never happened.
          
    Example:  SELECT [dbo].[LFS_ActiveMemberInDateRange] (67226, 4, '8-1-2018', '9-30-2018')  */

CREATE FUNCTION [dbo].[LFS_ActiveMemberInDateRange] (@customer_no INT, @memb_org INT, @start_dt DATETIME, @end_dt DATETIME) RETURNS CHAR(1)
AS BEGIN 

    /*  Function Variables  */

        DECLARE @active_member CHAR(1) = 'N'

        DECLARE @mem_table TABLE ([mem_no] INT, [mem_status] INT, [mem_org] INT, [mem_create] DATETIME, 
                                  [mem_init] DATETIME, [mem_expire] DATETIME)

    /*  Check Parameters  */

        --Set @check_dt to today if null and set time to 00:00:00
        SELECT @start_dt = ISNULL(@start_dt,GETDATE())
        SELECT @end_dt = ISNULL(@end_dt,GETDATE())

        SELECT @start_dt = CONVERT(DATE,@start_dt)
        SELECT @end_dt = CONVERT(DATE,@end_dt)

        --Set Default Values
        SELECT @memb_org = ISNULL(@memb_org,0)


    /*  If Custome No = 0, Jump to the End  */

        IF @customer_no = 0 GOTO FINISHED

    /*  Get all membership records for this customer - limit to specific organization if one was passed in
        Ignore records that aren't pending, active, or inactive - Remove times from all date/time values  */

        INSERT INTO @mem_table ([mem_no], [mem_status], [mem_org], [mem_create], [mem_init], [mem_expire])
            SELECT [cust_memb_no], 
                   [current_status], 
                   [memb_org_no], 
                   CONVERT(DATE,[create_dt]), 
                   CONVERT(DATE,[init_dt]), 
                   CONVERT(DATE,[expr_dt])
            FROM [dbo].[TX_CUST_MEMBERSHIP] 
            WHERE customer_no = @customer_no
              AND (@memb_org = 0 OR memb_org_no = @memb_org)
              AND current_status IN (1, 2, 3, 7)   --1=Inactive/2=Active/3=Pending/7=Lapsed

    /*  If no memberships found for this customer, jump to the end  */

        IF NOT EXISTS (SELECT * FROM @mem_table) GOTO FINISHED
 
    /* To determine if member that day, date passed must be between init date and expire date on an inactive, active, or pending membership  */

        IF EXISTS (SELECT * 
                   FROM @mem_table 
                   WHERE @start_dt BETWEEN [mem_init] AND [mem_expire] OR @end_dt BETWEEN [mem_init] AND [mem_expire])
            SELECT @active_member = 'Y'

    /*  Deal with a null value - Default to No */

        SELECT @active_member = ISNULL(@active_member,'N')

    FINISHED:

        /*  Return final value  */

            RETURN @active_member

END
GO

GRANT EXECUTE ON [dbo].[LFS_ActiveMemberInDateRange] TO ImpUsers
GO

