


ALTER FUNCTION [dbo].[LFT_ATTENDANCE_WEEK_DATES] (@init_dt DATETIME, @week_ends_on TINYINT, @offset_day_of_week CHAR(1))
        RETURNS @return_dates TABLE  ([init_date] DATETIME, 
                                      [cur_fiscal_year] INT,
                                      [cur_fiscal_start_dt] DATETIME,
                                      [cur_fiscal_end_dt] DATETIME, 
                                      [cur_week_start_dt] DATETIME, 
                                      [cur_week_start_day] VARCHAR(25),
                                      [cur_week_end_dt] DATETIME, 
                                      [cur_week_end_day] VARCHAR(25),
                                      [cur_month_start_dt] DATETIME,
                                      [cur_month_end_dt] DATETIME,
                                      [prv_fiscal] INT, 
                                      [prv_fiscal_start_dt] DATETIME,
                                      [prv_fiscal_end_dt] DATETIME,
                                      [prv_week_start_dt] DATETIME, 
                                      [prv_week_start_day] VARCHAR(25),
                                      [prv_week_end_dt] DATETIME,
                                      [prv_week_end_day] VARCHAR(25),
                                      [prv_month_start_dt] DATETIME,
                                      [prv_month_end_dt] DATETIME)
AS BEGIN

    DECLARE @cur_fiscal_year INT, @cur_fiscal_start_dt DATETIME, @cur_fiscal_end_dt DATETIME
    DECLARE @cur_week_end_dt DATETIME, @cur_week_start_dt DATETIME, @cur_month_end_dt DATETIME, @cur_month_start_dt DATETIME;
    DECLARE @prv_fiscal_year INT, @prv_fiscal_start_dt DATETIME, @prv_fiscal_end_dt DATETIME
    DECLARE @prv_week_end_dt DATETIME, @prv_week_start_dt DATETIME, @prv_month_end_dt DATETIME, @prv_month_start_dt DATETIME;
    
    DECLARE @extra_day_count INT = 0;

    /*  The cur_end_date = week_ending_dt and is initially set to the date passed to the function  */

        SELECT @cur_week_end_dt = @init_dt;

    /*  IF @week_ends_on = 0 then the week ends on value is whatever the day of the week is on the date passed to the function  */

        IF @week_ends_on = 0 
            SELECT @week_ends_on = DATEPART(WEEKDAY,@init_dt)

    /*  @week_ends_on cannot be > 7 - If >7 then set it to 7  */

        IF @week_ends_on > 7 
            SELECT @week_ends_on = 7

    /*  Make sure the day of the week of the current end date is the day the week ends on  */

        WHILE DATEPART(WEEKDAY,@cur_week_end_dt) <> @week_ends_on
            SELECT @cur_week_end_dt = DATEADD(DAY,1,@cur_week_end_dt);

    /* current start date = current end date - 6  */

        SELECT @cur_week_start_dt = DATEADD(DAY,-6,@cur_week_end_dt)

    /*  Fiscal year is same year as date if month is < 7 or year + 1 if month is >=7  */

        SELECT @cur_fiscal_year = DATEPART(YEAR,@cur_week_start_dt)

        IF DATEPART(MONTH,@cur_week_start_dt) >= 7
            SELECT @cur_fiscal_year += 1

    /*  Get current fiscal year start and end dates  */

        SELECT @cur_fiscal_start_dt = [dbo].[LF_GetFiscalYearStartDt](@cur_fiscal_year)
        SELECT @cur_fiscal_end_dt = [dbo].[LF_GetFiscalYearEndDt](@cur_fiscal_year)

    /*  Get curent month start and end date  */

        SELECT @cur_month_start_dt = FORMAT(@cur_week_start_dt,'yyyy/MM/01')

        SELECT @cur_month_end_dt= EOMONTH(@cur_month_start_dt)

    /*  Subtract one year from current start date to get previous start date  */

        SELECT @prv_week_start_dt = DATEADD(YEAR,-1,@cur_week_start_dt)

    /*  If offsetting previous date to compare days of the week rather than specific days, 
        set previous start date to the same day of the week as current start date  */

        IF @offset_day_of_week = 'Y' BEGIN

            WHILE DATEPART(WEEKDAY,@prv_week_start_dt) <> DATEPART(WEEKDAY,@cur_week_start_dt)
                SELECT @prv_week_start_dt = DATEADD(DAY,1,@prv_week_start_dt)

        END

    /*  Fiscal year is one year earlier  */

        SELECT @prv_fiscal_year = (@cur_fiscal_year - 1)

    /*  Get previous fiscal year start and end dates  */

        SELECT @prv_fiscal_start_dt = [dbo].[LF_GetFiscalYearStartDt](@prv_fiscal_year)
        SELECT @prv_fiscal_end_dt = [dbo].[LF_GetFiscalYearEndDt](@prv_fiscal_year)

    /*  Previous end date = previous start date + 6*/

        SELECT @prv_week_end_dt = DATEADD(DAY,6,@prv_week_start_dt)

    /*  Get previous month start and end date  */

        SELECT @prv_month_start_dt = FORMAT(@prv_week_start_dt,'yyyy/MM/01')

        SELECT @prv_month_end_dt = EOMONTH(@prv_month_start_dt)

    /*  If offsetting for day of week, slide start and end date of previous fiscal year's month by necessary number of days */

        IF @offset_day_of_week = 'Y' BEGIN

            SELECT @extra_day_count = 0

            WHILE DATEPART(WEEKDAY,@prv_month_start_dt) <> DATEPART(WEEKDAY,@cur_month_start_dt) BEGIN
                SELECT @prv_month_start_dt = DATEADD(DAY,1,@prv_month_start_dt)
                SELECT @extra_day_count += 1
            END

            SELECT @prv_month_end_dt = DATEADD(DAY,@extra_day_count,@prv_month_end_dt)

        END

    /*  Insert values into the return table and then RETURN  */

        INSERT INTO @return_dates
        SELECT @init_dt,
               @cur_fiscal_year,
               @cur_fiscal_start_dt,
               @cur_fiscal_end_dt,
               @cur_week_start_dt,
               DATENAME(WEEKDAY,@cur_week_start_dt),
               CONVERT(CHAR(10),@cur_week_end_dt,111) + ' 23:59:59.957',
               DATENAME(WEEKDAY,@cur_week_end_dt),
               @cur_month_start_dt,
               @cur_month_end_dt,
               @prv_fiscal_year,
               @prv_fiscal_start_dt,
               @prv_fiscal_end_dt,
               @prv_week_start_dt,
               DATENAME(WEEKDAY, @prv_week_start_dt),
               CONVERT(CHAR(10), @prv_week_end_dt,111) + ' 23:59:59.957',
               DATENAME(WEEKDAY,@prv_week_end_dt),
               @prv_month_start_dt,
               @prv_month_end_dt

    FINISHED:

        RETURN

END    
GO



SELECT * FROM [dbo].[LFT_ATTENDANCE_WEEK_DATES] ('3-21-2019', 1, 'Y')

