USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[LFT_BOARD_GetCorporateMembership]
(
	@customer_no INT 
)
RETURNS @MembershipTbl TABLE 
(
	CorporateMembType VARCHAR(30),
	CorporateMembExpirationDate datetime
)
AS 
BEGIN

DECLARE @EmployerCustomerNo INT

SELECT @EmployerCustomerNo = a.group_customer_no
FROM dbo.T_AFFILIATION a
INNER JOIN dbo.TR_AFFILIATION_TYPE at
	ON at.id = a.affiliation_type_id
	AND at.description = 'Employee'
	AND a.individual_customer_no = @customer_no ;

INSERT INTO @MembershipTbl (CorporateMembType, CorporateMembExpirationDate)
SELECT memb_level_description, expiration_date 
FROM dbo.LV_CURRENT_MEMBERSHIP_INFO 
WHERE customer_no IN (SELECT expanded_customer_no FROM dbo.V_CUSTOMER_WITH_PRIMARY_GROUP WHERE customer_no = @EmployerCustomerNo)

RETURN;

END 