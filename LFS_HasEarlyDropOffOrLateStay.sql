USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

--IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LFS_HasEarlyDropOffOrLateStay]') AND type IN (N'FN', N'IF', N'TF', N'FS', N'FT'))
--    DROP FUNCTION [dbo].[LFS_HasEarlyDropOffOrLateStay]
--GO

/*  pass it a course date, a recipient number and what you want to know and the function will return Yes or No.
    @early_late can be "Early", "Late" or "Both"  If asking for both, function will return yes only 
    if participant has both early drop-off and late stay for that particular day.  */

CREATE FUNCTION [dbo].[LFS_HasEarlyDropOffOrLateStay](@course_dt DATETIME, @participant_no INT, @early_late VARCHAR(10)) RETURNS VARCHAR(3) 
AS BEGIN 

    /*  Function Variables  */

        DECLARE @Yes_No VARCHAR(3) = 'No'
        DECLARE @perf_no_early INT = 0, @zone_no_early INT = 0, @has_early VARCHAR(3) = 'No',
                @perf_no_late INT = 0, @zone_no_late INT = 0, @has_late varchar(3) = 'No'


    /*  Remove time piece of the course date (time set to 00:00:00  */

        SELECT @course_dt = CONVERT(DATE,@course_dt)
    
    /*  Get performance and zone numbers for the early drop-off performance for that course date
        Should only be one per date but if multiple are found, it takes the highest numbers  */

        IF @early_late IN ('Early','Both')
            SELECT @perf_no_early = ISNULL(MAX([performance_no]),0), @zone_no_early = ISNULL(MAX([performance_zone]),0)
            FROM dbo.LV_PRODUCTION_ELEMENTS_PERFORMANCE 
            WHERE CONVERT(DATE,[performance_dt]) = @course_dt AND [production_name] = 'Early Drop-Off'

    /*  Get performance and zone numbers for the late stay performance for that course date
        Should only be one per date but if multiple are found, it takes the highest numbers  */

        IF @early_late IN ('Late','Both')
            SELECT @perf_no_late = ISNULL(MAX([performance_no]),0), @zone_no_late = ISNULL(MAX([performance_zone]),0)
            FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] (NOLOCK)
            WHERE CONVERT(DATE,[performance_dt]) = @course_dt AND [production_name] = 'Late Stay'

    /*  Determine whether or not the participant has early drop-off that day (if needed)  */

        IF @perf_no_early > 0 AND @zone_no_early > 0 BEGIN
            IF EXISTS (SELECT * FROM dbo.T_SUB_LINEITEM (NOLOCK)
                       WHERE recipient_no = @participant_no AND perf_no = @perf_no_early AND zone_no = @zone_no_early AND sli_status IN (3,12))
                SELECT @has_early = 'Yes'
        END

    /*  Determine whether or not the participant has early late stay that day (if needed)  */

        IF @perf_no_late > 0 AND @zone_no_late > 0 BEGIN
            IF EXISTS (SELECT * FROM [dbo].[T_SUB_LINEITEM] (NOLOCK)
                       WHERE [recipient_no] = @participant_no AND [perf_no] = @perf_no_late AND [zone_no] = @zone_no_late AND [sli_status] IN (3,12))
                SELECT @has_late = 'Yes'
        END

    FINISHED:


        IF @early_late = 'Early' AND @has_early = 'Yes' SELECT @Yes_No = 'Yes'

        ELSE IF @early_late = 'Late' AND @has_late = 'Yes' SELECT @Yes_No = 'Yes'

        ELSE IF @early_late = 'Both' AND @has_early = 'Yes' AND @has_late = 'Yes' SELECT @Yes_No = 'Yes'

        ELSE SELECT @Yes_No = 'No'


        RETURN ISNULL(@Yes_No,'No')

END
GO

GRANT EXECUTE ON [dbo].[LFS_HasEarlyDropOffOrLateStay] TO ImpUsers
GO
