USE [impresario]
GO

/****** Object:  UserDefinedFunction [dbo].[LF_GetFiscalYear]    Script Date: 8/25/2018 11:31:30 AM ******/
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LF_GetFiscalQuarter]') AND type IN (N'FN', N'IF', N'TF', N'FS', N'FT'))
    DROP FUNCTION [dbo].[LF_GetFiscalQuarter]
GO

CREATE FUNCTION [dbo].[LF_GetFiscalQuarter] (@fDate DATETIME = NULL) RETURNS INTEGER
AS BEGIN

    DECLARE @fiscal_year INT = 0, @fiscal_quarter INT = 0

    IF @fDate IS NOT NULL BEGIN
    
        SELECT @fiscal_year = MAX([fyear]) FROM [dbo].[TR_Batch_Period] WHERE @fDate BETWEEN [start_dt] AND [end_dt]
        SELECT @fiscal_quarter = MAX([quarter]) FROM [dbo].[TR_Batch_Period] WHERE [fyear] = @fiscal_year and @fDate BETWEEN [start_dt] AND [end_dt]

    END

    RETURN @fiscal_quarter

END
GO

GRANT EXECUTE ON [dbo].[LF_GetFiscalQuarter] TO [ImpUsers] AS [dbo]
GO


SELECT dbo.LF_GetFiscalYear(GETDATE())
SELECT dbo.LF_GetFiscalQuarter(GETDATE())


