


        ALTER FUNCTION [dbo].[LFS_PERF_TITLE_NO] (@perf_no AS INT) RETURNS INT
        AS BEGIN
            
            DECLARE @title_no AS INT = 0

            SELECT @perf_no = ISNULL(@perf_no,0)

            IF @perf_no >0
                SELECT @title_no = ISNULL(ttl.[title_no],0)
                FROM dbo.T_PERF AS prf (NOLOCK)
                     INNER JOIN dbo.T_PROD_SEASON AS sea (NOLOCK) ON sea.[prod_season_no] = prf.[prod_season_no]
                     INNER JOIN dbo.T_PRODUCTION AS pro (NOLOCK) ON pro.[prod_no] = sea.[prod_no]
                     INNER JOIN dbo.T_TITLE AS ttl (NOLOCK) ON ttl.[title_no] = pro.[title_no]
                WHERE prf.perf_no = @perf_no


            RETURN @title_no

        END
        GO




   