DROP FUNCTION [dbo].[LFT_GetFiscalYears]
GO

CREATE FUNCTION [dbo].[LFT_GetFiscalYears] (@num_of_prev_years INT, @include_current CHAR(1)) 
                RETURNS @fiscal_years TABLE  ([fiscal_year_order] INT, [fiscal_year] INT)
AS BEGIN

    DECLARE @order INT = 1;

    IF @include_current = 'Y' BEGIN
        INSERT INTO @fiscal_years ([fiscal_year_order], [fiscal_year])
        SELECT 0, MAX(fyear) FROM [dbo].[TR_Batch_Period] WHERE GETDATE() BETWEEN [start_dt] AND [end_dt];
    END;

    WHILE @order <= @num_of_prev_years BEGIN

        INSERT INTO @fiscal_years ([fiscal_year_order], [fiscal_year])
        SELECT @order, MAX(fyear) 
        FROM [dbo].[TR_Batch_Period] 
        WHERE DATEADD(YEAR, (@order * -1), GETDATE()) BETWEEN [start_dt] AND [end_dt]

        SELECT @order += 1

    END;

    DELETE FROM @fiscal_years WHERE ISNULL([fiscal_year],0) = 0
            
    FINISHED:

        RETURN

END
GO

GRANT SELECT ON [dbo].[LFT_GetFiscalYears] TO [ImpUsers], [tessitura_app]
GO

SELECT [fiscal_year] FROM [dbo].[LFT_GetFiscalYears](10, 'Y') ORDER BY [fiscal_year_order]

