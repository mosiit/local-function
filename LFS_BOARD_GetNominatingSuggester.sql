USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create FUNCTION [dbo].[LFS_BOARD_GetNominatingSuggester](@customer_no INT, @BoardType VARCHAR(30))
RETURNS VARCHAR(100) AS

BEGIN 

	DECLARE @NominatingSuggester VARCHAR(100) = '';
	
	SELECT TOP 1 @NominatingSuggester = cust.display_name
	FROM T_ASSOCIATION asso
	INNER JOIN dbo.FT_CONSTITUENT_DISPLAY_NAME() AS cust 
			ON asso.customer_no = cust.customer_no
	WHERE asso.customer_no IN (SELECT expanded_customer_no FROM dbo.V_CUSTOMER_WITH_PRIMARY_GROUP WHERE customer_no = @customer_no)
	AND association_type_id = CASE @BoardType 
		WHEN 'Active Trustee' THEN 89
		WHEN 'Active Overseer' THEN 86
		END 
	ORDER BY start_dt DESC

	RETURN @NominatingSuggester

END 
GO
