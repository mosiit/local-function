USE [impresario]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[LFT_BOARD_GetAddress]
(
	@customer_no INT,
	@AddressType VARCHAR(20), -- Additional1, Additional2, Seasonal1, Seasonal2, Home, Business
	@Handbook INT 
)
RETURNS @AddressTbl TABLE 
(
	Address1 VARCHAR(100),
	Address2 VARCHAR(100),
	Address3 VARCHAR(100),
	City VARCHAR(100),
	StateCd VARCHAR(2),
	ZipCode VARCHAR(10),
	Phone VARCHAR(20),
	Fax VARCHAR(20),
	Notes VARCHAR(500)
)
AS 
BEGIN

DECLARE @addressTypeID INT 

SELECT @addressTypeID = id
FROM dbo.TR_ADDRESS_TYPE
WHERE description = CASE @AddressType WHEN 'Business' THEN 'Business Mailing' ELSE @AddressType END ;

WITH NotesCTE
AS 
(
	SELECT cpp.contact_point_id, purpose.description purpose, cat.description category, cpp.purpose_id
	FROM dbo.TX_CONTACT_POINT_PURPOSE cpp
	INNER JOIN dbo.TR_CONTACT_POINT_PURPOSE purpose
		ON purpose.id = cpp.purpose_id
	INNER JOIN dbo.TR_CONTACT_POINT_CATEGORY cat
		ON cat.id = cpp.contact_point_category
	WHERE purpose.inactive ='N'
)
INSERT INTO @AddressTbl
(
	Address1,
	Address2,
	Address3,
	City,
	StateCd,
	ZipCode,
	Phone,
	Fax,
	Notes
)
SELECT TOP 1 
	ISNULL(addr.street1, ' ') AS street1,
	ISNULL(addr.street2, ' ') AS street2,
	ISNULL(addr.street3, ' ') AS street3,
	ISNULL(addr.city, ' ') AS city,
	ISNULL(addr.state, ' ') AS state,
	dbo.AF_FORMAT_STRING(addr.postal_code, '@@@@@-@@@@') AS PostalCode,
	CASE @AddressType
		WHEN 'Business' THEN dbo.LFS_BOARD_GetBoardBusinessPhone(@customer_no, @Handbook)
		ELSE dbo.LFS_BOARD_GetBoardPhoneByType(@customer_no, @Handbook, @AddressType)
	END, 
	dbo.LFS_BOARD_GetBoardPhoneByType(@customer_no, @Handbook, @AddressType + ' Fax'),
	ISNULL('Address CPP: ' + n.purpose, ' ') AS notes
FROM dbo.T_ADDRESS addr
	INNER JOIN dbo.V_CUSTOMER_WITH_PRIMARY_AFFILIATES pri
		ON addr.customer_no = pri.expanded_customer_no
	LEFT JOIN NotesCTE n
		ON addr.address_no = n.contact_point_id
		AND n.category = 'Postal Address'
	WHERE pri.customer_no = @customer_no 
		AND addr.address_type = @addressTypeID
		AND addr.inactive = 'N'
ORDER BY ISNULL(addr.street1, ' '),
	ISNULL(addr.street2, ' '),
	ISNULL(addr.street3, ' '),
	ISNULL(addr.city, ' '),
	ISNULL(addr.state, ' '),
	ISNULL(addr.postal_code, ' ')

RETURN;

END 

GO


