
ALTER FUNCTION [dbo].[LFT_GetCalendarYears] (@num_of_prev_years INT, @num_of_future_years INT, @include_current CHAR(1)) 
                RETURNS @Calendar_years TABLE  ([calendar_year_order] INT, [calendar_year] INT)
AS BEGIN

    DECLARE @order INT = 1;
    DECLARE @start_date DATETIME, @end_date datetime

    SELECT @start_date = DATEADD(YEAR,(@num_of_prev_years * -1),GETDATE())
    SELECT @end_date = DATEADD(YEAR,@num_of_future_years,GETDATE())

    WHILE @start_date <= @end_date BEGIN

        INSERT INTO @Calendar_years ([calendar_year_order],[calendar_year])
        VALUES (@order, DATEPART(YEAR,@start_date))

        SELECT @start_date = DATEADD(YEAR, 1, @start_date)

        SELECT @order = (@order + 1)

    END;
      
    IF @include_current = 'N'
        DELETE FROM @Calendar_years 
        WHERE [calendar_year] = DATEPART(YEAR,GETDATE());

    FINISHED:

        RETURN

END
GO

GRANT SELECT ON [dbo].[LFT_GetCalendarYears] TO [ImpUsers], [tessitura_app]
GO

SELECT [calendar_year] FROM [dbo].[LFT_GetCalendarYears] (2, 0, 'Y') ORDER BY [calendar_year_order]
SELECT [calendar_year] FROM [dbo].[LFT_GetCalendarYears] (2, 0, 'N') ORDER BY [calendar_year_order]
SELECT [calendar_year] FROM [dbo].[LFT_GetCalendarYears] (2, 5, 'Y') ORDER BY [calendar_year_order]
SELECT [calendar_year] FROM [dbo].[LFT_GetCalendarYears] (0, 5, 'N') ORDER BY [calendar_year_order]
