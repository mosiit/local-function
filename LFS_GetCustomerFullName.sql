USE [impresario]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

--IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LFS_GetCustomerFullName]') AND type IN (N'FN', N'IF', N'TF', N'FS', N'FT'))
--    DROP FUNCTION [dbo].[LFS_GetCustomerFullName]
--GO

/*  pass it a customer numner and it will return the customer's full name using the first, middle, and last name values  */

CREATE FUNCTION [dbo].[LFS_GetCustomerFullName](@customer_no INT) RETURNS VARCHAR(150) 
AS BEGIN 

    DECLARE @cus_full_name VARCHAR(150), @cus_first_name VARCHAR(50), @cus_middle_name VARCHAR(50), @cus_last_name VARCHAR(50)

    SELECT @cus_first_name = fname,
           @cus_middle_name = mname,
           @cus_last_name = lname
    FROM [dbo].[T_CUSTOMER] (NOLOCK)
    WHERE [customer_no] = @customer_no


    SELECT @cus_full_name = ISNULL(@cus_first_name,'') + ' '
                          + CASE WHEN ISNULL(@cus_middle_name,'') = '' THEN '' ELSE @cus_middle_name + ' ' END
                          + ISNULL(@cus_last_name,'')

    RETURN ISNULL(LTRIM(RTRIM(@cus_full_name)),'')

END
GO

GRANT EXECUTE ON [dbo].[LFS_GetCustomerFullName] TO ImpUsers
GO

