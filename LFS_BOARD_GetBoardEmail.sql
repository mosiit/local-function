USE [impresario]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[LFS_BOARD_GetBoardEmail](@customer_no INT, @Handbook INT)
RETURNS VARCHAR(100) AS

BEGIN 

-- Only want to pull Email information from the INDIVIDUAL record (not Household) where the CPP = Board Information
-- If @Handbook = 1 (being called from LRP_BOARD_ActiveBoardHandbook) and CPP = "Do Not Publish in Handbook", then return a blank
-- Otherwise, pull the Email record (top 1)  where the CPP = Board Information

	DECLARE @email VARCHAR(100) = '';

	WITH NotesCTE
	AS 
	(
		SELECT cpp.contact_point_id, purpose.description purpose, cat.description category, cpp.purpose_id
		FROM dbo.TX_CONTACT_POINT_PURPOSE cpp
		INNER JOIN dbo.TR_CONTACT_POINT_PURPOSE purpose
			ON purpose.id = cpp.purpose_id
		INNER JOIN dbo.TR_CONTACT_POINT_CATEGORY cat
			ON cat.id = cpp.contact_point_category
		WHERE purpose.id = 15 -- Do Not Publish in Handbook
			AND cat.id = -2 -- Email Address
			AND purpose.inactive = 'N'
	)
	SELECT TOP 1 @email = CASE WHEN @Handbook = 1 AND n.purpose_id  = 15 THEN '' ELSE ISNULL(eadd.address , '') END 
	FROM dbo.T_EADDRESS eadd
	INNER JOIN dbo.TX_CONTACT_POINT_PURPOSE cpp
		ON cpp.contact_point_id = eadd.eaddress_no
	INNER JOIN dbo.TR_CONTACT_POINT_PURPOSE purpose
		ON purpose.id = cpp.purpose_id
		AND purpose.inactive = 'N'
	INNER JOIN dbo.TR_CONTACT_POINT_CATEGORY cat
		ON cat.id = cpp.contact_point_category
	LEFT JOIN NotesCTE n
		ON n.contact_point_id = eadd.eaddress_no
	WHERE cat.description = 'Email Address' AND purpose.description = 'Board Information'
		AND eadd.eaddress_type IN (1, 5) -- Primary Email Address, Business
		AND eadd.inactive = 'N'
		AND eadd.customer_no = @customer_no

RETURN @email 

END

GO