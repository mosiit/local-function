USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LF_GetPricingRuleMessage]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
    DROP FUNCTION [dbo].[LF_GetPricingRuleMessage]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[LF_GetPricingRuleMessage] (@perf_no INT = 0) RETURNS VARCHAR(8000)
AS BEGIN

    DECLARE @return_message VARCHAR(8000) = ''
    DECLARE @qual_rule_id INT = 0, @rule_Id INT = 0, @message_id INT = 0
    DECLARE @qual_perf VARCHAR(8000)
    DECLARE @perf_table TABLE ([rule_id] INT, [perf_no] INT)
    
    /*  Create a tabe of all performances that have early close rules attached to them
        Unfortunately, the qualifying performances are stored in a comma separated list, instead of a TX table,
        requiring that they all be parsed to deterimine if the performance number exists  */

        DECLARE perf_cursor INSENSITIVE CURSOR FOR 
        SELECT [id], [qualifying_performance] FROM [dbo].[T_PRICING_RULE] WHERE [description] LIKE '%Early Close%'
        OPEN perf_cursor
        BEGIN_PERF_LOOP:

            FETCH NEXT FROM perf_cursor INTO @qual_rule_id, @qual_perf
            IF @@FETCH_STATUS = -1 GOTO END_PERF_LOOP

            INSERT INTO @perf_table ([rule_id], [perf_no])
            SELECT @qual_rule_id, [item] FROM [dbo].[LFT_SPLIT_DILEMETED_STRING](@qual_perf,',')

            GOTO BEGIN_PERF_LOOP

        END_PERF_LOOP:
        CLOSE perf_cursor
        DEALLOCATE perf_cursor

    /*  If the passed performance number does not exist in the table, jump to the end  */

        IF NOT EXISTS (SELECT * FROM @perf_table WHERE perf_no = @perf_no) GOTO FINISHED


    /*  Find the highest rule id attached to the passed performance  */
    
        SELECT @rule_Id = MAX([rule_id]) FROM @perf_table WHERE perf_no = @perf_no

    /*  If nothing found, jump to the end  */

        SELECT @rule_id = ISNULL(@rule_id,0)
        IF @rule_Id = 0 GOTO FINISHED

    /*  Find the highest message id attached to the rule id  */

        SELECT @message_id = MAX([ID]) FROM dbo.T_PRICING_RULE_MESSAGE (NOLOCK) WHERE [rule_id] = @rule_Id

    /*  If nothing found, jump to the end  */

        SELECT @message_id = ISNULL(@message_id,0)

        IF @message_id = 0 GOTO FINISHED

    /*  Pull the message text from the T_PRICING_RULE_MESSATE TABLE  */

        SELECT @return_message = [message] FROM dbo.T_PRICING_RULE_MESSAGE (NOLOCK) WHERE [id] = @message_id

    /*  If nothing found, clear the return variable  */

        SELECT @return_message = ISNULL(@return_message,'')

    FINISHED:

        /*  Return the pricing rule message (if one was found)  */

            RETURN @return_message

END
GO
    
GRANT EXECUTE ON [dbo].[LF_GetPricingRuleMessage] TO ImpUsers
GO
